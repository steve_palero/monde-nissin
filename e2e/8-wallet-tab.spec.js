describe('Wallet flow', () => {
  it('should login successfully', async () => {
    await device.reloadReactNative();
    await expect(element(by.id('email'))).toExist();
    await element(by.id('email')).clearText();
    await element(by.id('email')).typeText('user');
    await element(by.id('password')).clearText();
    await element(by.id('password')).typeText('123');
    await element(by.id('password')).tapReturnKey();
    await element(by.id('Login')).tap();
  })

  it('should navigate to wallet tab successfully', async () => {
    await waitFor(element(by.id('walletTab'))).toBeVisible().withTimeout(5000);
    await element(by.id('walletTab')).tap();
  })

  it('should display list of transactions', async () => {
    await waitFor(element(by.id('transactionsAll'))).toBeVisible().withTimeout(5000);
    await element(by.id('transactionsAll')).tap();
    await waitFor(element(by.id('transactionsList'))).toBeVisible().withTimeout(10000);
    await element(by.id('transactionsList')).swipe('up');
    await element(by.id('transactionsBack')).tap();
  })

  it('should logout successfully', async () => {
    await waitFor(element(by.id('headerLeftButton'))).toBeVisible().withTimeout(5000);
    await element(by.id('headerLeftButton')).tap();
    await element(by.text('Logout')).tap();
    await element(by.id('logout')).tap();
    await expect(element(by.id('Login'))).toBeVisible();
  })
})