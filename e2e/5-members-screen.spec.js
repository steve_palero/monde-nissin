describe('Members screen flow', () => {
  it('should login successfully', async () => {
    await device.reloadReactNative();
    await expect(element(by.id('email'))).toExist();
    await element(by.id('email')).clearText();
    await element(by.id('email')).typeText('user');
    await element(by.id('password')).clearText();
    await element(by.id('password')).typeText('123');
    await element(by.id('password')).tapReturnKey();
    await element(by.id('Login')).tap();
  })

  it('should navigate to members screen successfully', async () => {
    await waitFor(element(by.id('headerLeftButton'))).toBeVisible().withTimeout(5000);
    await element(by.id('headerLeftButton')).tap();
    await expect(element(by.id('drawerMembers'))).toExist();
    await element(by.id('drawerMembers')).tap();
    await expect(element(by.id('membersList'))).toBeVisible();
    await expect(element(by.id('membersAddNew'))).toExist();
    await element(by.id('membersAddNew')).tap();
  })

  it('should navigate to add new member screen successfully', async () => {
    await waitFor(element(by.id('addNewMemberNameInput'))).toExist().withTimeout(5000)
    await element(by.id('addNewMemberNameInput')).typeText('Steve Palero')
    await element(by.id('addNewMemberNameInput')).tapReturnKey();
    await expect(element(by.id('addNewMemberAddressInput'))).toBeVisible();
    await element(by.id('addNewMemberAddressInput')).typeText('#4455 San Antonio St., Brgy. Bangkal, Makati City')
    await element(by.id('addNewMemberAddressInput')).tapReturnKey();
    await expect(element(by.id('addNewMemberMobileInput'))).toBeVisible();
    await element(by.id('addNewMemberMobileInput')).typeText('09675542851');
    await element(by.id('addNewMemberMobileInput')).tapReturnKey();
    await expect(element(by.id('addNewMemberBirthdayInput'))).toBeVisible();
    await element(by.id('addNewMemberBirthdayInput')).typeText('06/23/1998');
    await element(by.id('addNewMemberBirthdayInput')).tapReturnKey();
    await expect(element(by.id('addNewMemberSubmit'))).toBeVisible();
    await element(by.id('addNewMemberSubmit')).tap();
    await element(by.id('addNewMemberBack')).tap();
    await expect(element(by.id('membersHeaderBack'))).toBeVisible();
    await element(by.id('membersHeaderBack')).tap();
    await element(by.text('Logout')).tap();
    await element(by.id('logout')).tap();
    await expect(element(by.id('Login'))).toBeVisible();
  })
})