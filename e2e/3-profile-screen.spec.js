describe('Profile screen flow', () => {
  it('should login successfully', async () => {
    await device.reloadReactNative();
    await expect(element(by.id('email'))).toExist();
    await element(by.id('email')).clearText();
    await element(by.id('email')).typeText('user');
    await element(by.id('password')).clearText();
    await element(by.id('password')).typeText('123');
    await element(by.id('password')).tapReturnKey();
    await element(by.id('Login')).tap();
  })
  
  it('should navigate to profile screen successfully', async () => {
    await waitFor(element(by.id('headerLeftButton'))).toExist().withTimeout(5000);
    await element(by.id('headerLeftButton')).tap();
    await expect(element(by.id('drawerProfile'))).toExist();
    await element(by.id('drawerProfile')).tap();
    await expect(element(by.id('profileContent'))).toExist();
    await element(by.id('profileScrollView')).swipe('up');
    await element(by.id('profileHeaderLeftButton')).tap();
    await element(by.text('Logout')).tap();
    await element(by.id('logout')).tap();
    await expect(element(by.id('Login'))).toBeVisible();
  })
})