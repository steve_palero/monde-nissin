describe('Transaction flow', () => {
  it('should login successfully', async () => {
    await device.reloadReactNative();
    await expect(element(by.id('email'))).toExist();
    await element(by.id('email')).clearText();
    await element(by.id('email')).typeText('user');
    await element(by.id('password')).clearText();
    await element(by.id('password')).typeText('123');
    await element(by.id('password')).tapReturnKey();
    await element(by.id('Login')).tap();
  });

  it('should cancel member purchase', async () => {
    await waitFor(element(by.id('memberTransaction'))).toExist().withTimeout(5000);
    await element(by.id('memberTransaction')).tap();
    await expect(element(by.id('memberTransactionModal'))).toExist();
    await element(by.id('memberTransactionCancel')).tap();
    await expect(element(by.id('transactionAlertIcon'))).toBeVisible();
    await element(by.id('transactionAlertOK')).tap();
    await expect(element(by.id('transactionAlertIcon'))).toBeNotVisible();
  })

  it('member purchase should be successful', async () => {
    await element(by.id('memberTransaction')).tap();
    await expect(element(by.id('memberTransactionModal'))).toExist();
    await element(by.id('memberPurchaseQuantityInput')).typeText('2');
    await element(by.id('memberTransactionSave')).tap();
    await expect(element(by.id('transactionAlertIcon'))).toBeVisible();
    await element(by.id('transactionAlertOK')).tap();
    await expect(element(by.id('transactionAlertIcon'))).toBeNotVisible();
  })

  it('should cancel walk-in purchase', async () => {
    await expect(element(by.id('walkInTransaction'))).toExist();
    await element(by.id('walkInTransaction')).tap();
    await expect(element(by.id('walkInTransactionModal'))).toExist();
    await element(by.id('walkInTransactionCancel')).tap();
    await expect(element(by.id('transactionAlertIcon'))).toBeVisible();
    await element(by.id('transactionAlertOK')).tap();
    await expect(element(by.id('transactionAlertIcon'))).toBeNotVisible();
  })

  it('walk-in purchase should be successful', async () => {
    await element(by.id('walkInTransaction')).tap();
    await expect(element(by.id('walkInTransactionModal'))).toExist();
    await element(by.id('walkInPurchaseQuantityInput')).typeText('5')
    await element(by.id('walkInTransactionSave')).tap();
    await expect(element(by.id('transactionAlertIcon'))).toBeVisible();
    await element(by.id('transactionAlertOK')).tap();
    await expect(element(by.id('transactionAlertIcon'))).toBeNotVisible();
  })

  it('should logout successfully', async () => {
    await expect(element(by.id('headerLeftButton'))).toExist();
    await element(by.id('headerLeftButton')).tap();
    await element(by.text('Logout')).tap();
    await element(by.id('logout')).tap();
    await expect(element(by.id('Login'))).toBeVisible();
  })
})