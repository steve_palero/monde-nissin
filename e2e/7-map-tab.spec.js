describe('Map flow', () => {
  it('should login successfully', async () => {
    await device.reloadReactNative();
    await expect(element(by.id('email'))).toExist();
    await element(by.id('email')).clearText();
    await element(by.id('email')).typeText('user');
    await element(by.id('password')).clearText();
    await element(by.id('password')).typeText('123');
    await element(by.id('password')).tapReturnKey();
    await element(by.id('Login')).tap();
  })

  it('should navigate to map tab successfully', async () => {
    await waitFor(element(by.id('mapTab'))).toBeVisible().withTimeout(5000);
    await element(by.id('mapTab')).tap();
  })

  it('should render covered area', async () => {
    await waitFor(element(by.id('showCoveredArea'))).toBeVisible().withTimeout(3000);
    await element(by.id('showCoveredArea')).tap();
    await waitFor(element(by.id('coveredArea'))).toBeVisible();
  })

  it('should unrender covered area', async () => {
    await waitFor(element(by.id('showCoveredArea'))).toBeVisible().withTimeout(3000);
    await element(by.id('showCoveredArea')).tap();
    await waitFor(element(by.id('coveredArea'))).toBeNotVisible();
  })

  it('should logout successfully', async () => {
    await waitFor(element(by.id('headerLeftButton'))).toBeVisible().withTimeout(5000);
    await element(by.id('headerLeftButton')).tap();
    await element(by.text('Logout')).tap();
    await element(by.id('logout')).tap();
    await expect(element(by.id('Login'))).toBeVisible();
  })
})