describe('Inventory flow', () => {
  it('should login successfully', async () => {
    await device.reloadReactNative();
    await expect(element(by.id('email'))).toExist();
    await element(by.id('email')).clearText();
    await element(by.id('email')).typeText('user');
    await element(by.id('password')).clearText();
    await element(by.id('password')).typeText('123');
    await element(by.id('password')).tapReturnKey();
    await element(by.id('Login')).tap();
  })

  it('should navigate to inventory screen', async () => {
    await waitFor(element(by.id('headerLeftButton'))).toBeVisible().withTimeout(5000);
    await element(by.id('headerLeftButton')).tap();
    await expect(element(by.id('drawerInventory'))).toExist();
    await element(by.id('drawerInventory')).tap();
    await expect(element(by.id('inventoryList'))).toBeVisible();
    await expect(element(by.id('inventoryBack'))).toBeVisible();
    await element(by.id('inventoryBack')).tap();
    await element(by.text('Logout')).tap();
    await element(by.id('logout')).tap();
    await expect(element(by.id('Login'))).toBeVisible();
  })
})