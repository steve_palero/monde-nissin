import React, { PureComponent, Component } from 'react'

import {
  StyleProp,
  ViewStyle,
  TextStyle,
  ImageStyle,
  TextInputProps,
  TextInputIOSProps,
  ScrollViewProps,
  FlatListProps,
  ViewProps,
  TextProps,
} from 'react-native'

import { LinearGradientProps } from 'react-native-linear-gradient'

import { KeyboardAwareProps } from 'react-native-keyboard-aware-scroll-view'
import { FastImageSource } from 'react-native-fast-image'

type ImageStyleProp = StyleProp<ImageStyle>

type Boolean = true | false

type gradientPositions = {
  x: number,
  y: number,
}

type fontTypes = 'light' | 'bold' | 'regular' | 'lightItalic' | 'italic' | 'boldItalic'

type resizeModes = 'contain' | 'cover' | 'stretch' | 'center'

type priority = 'high' | 'normal' | 'low'

type cache = 'immutable' | 'web' | 'cacheOnly'

/**
 * CachedImage exclusive prop
 */
type authorization = {
  Authorization?: String,
}

/**
 * CachedImage exclusive prop
 */
type customImageCache = {
  uri: String,
  header?: authorization,
  priority?: priority,
  cache?: cache,
}

export interface ContainerProps extends ViewProps {
  gradient?: Boolean,
  /**
   * { x: number, y: number }
   */
  start?: gradientPositions,
  /**
   * { x: number, y: number }
   */
  end?: gradientPositions,
  colors?: Array<any>,
  style?: StyleProp<ViewStyle>,
  children?: Element,
  drawer?: Boolean,
  drawerContent?: Element,
  drawerPercentage?: Number,
  drawerOverlay?: Boolean,
  drawerOpacity?: Number,
}
/**
 * Container
 */
export class Container extends PureComponent<ContainerProps> { }

export interface CachedImageProps {
  /**
   * Source image
   * 
   * Can be a local image or remote image
   */
  uri: customImageCache | FastImageSource,
  /**
   * width of the image
   * 
   * defaults to 20
   */
  width?: number,
  /**
   * height of the image
   * 
   * defaults to 20
   */
  height?: number,
  /**
   * Specifies if image should have rounded border or not
   */
  rounded?: Boolean,
  bordered?: Boolean,
  borderWidth?: Number,
  borderColor?: String,
  /**
   * style of the image.
   */
  imageStyle?: ImageStyleProp,
  /**
   * resizeMode of the image.
   */
  resize?: resizeModes,
  /**
   * tintColor of the image,
   */
  tintColor?: String,
}

/**
 * CachedImage
 * 
 * Custom image component.
 * 
 * Made with react-native-fast-image.
 */
export class CachedImage extends PureComponent<CachedImageProps> { }

export interface ParagraphProps extends TextProps {
  /**
    * This can be one of the following values:
    *
    * - `light` -  Thinner font
    * - `bold` - Heavier font
    * - `regular` - Common font
    * - `lightItalic` - Thinner Italic font
    * - `italic` - Italic font
    * - `boldItalic` - Heavier Italic font
    * 
    * > See `src/config/themes/Fonts.js` for font values.
    * 
    * > The default is `regular`.
    */
  fontType?: fontTypes;
  size?: number;
  color?: String;
  /**
   * Required prop!
   */
  text: String | number;
  center?: Boolean;
  textStyle?: object;
  tappable?: Boolean;
  onTap?: Function;
}

/**
 * Paragraph
 *
 * Simplified Text component.
 * 
 * See https://react-native-halcyon.github.io/documentation/docs/components/paragraph
 */
export class Paragraph extends PureComponent<ParagraphProps> { }

export interface SpaceProps {
  horizontal?: true | false,
  size?: number,
  /**
   * Background of the space.
   * 
   * Used as if like a border.
   */
  color?: String,
}

/**
 * Space
 *
 * UI spacer. Horizontal / Vertical
 * 
 * Also works as a border if you pass the `color` prop.
 * 
 */
export class Space extends PureComponent<SpaceProps> { }

export interface ImageBackgroundProps {
  style: StyleProp<ViewStyle>;
  uri: customImageCache;
  /**
   * Determines how to resize the image when the frame doesn't match the raw image dimensions.
   * 
   * This can be one of the following values:
   * - `contain` - Scale the image uniformly (maintain the image's aspect ratio) so that both dimensions (width and height) of the image will be equal to or less than the corresponding dimension of the view (minus padding).
   * - `cover` - Scale the image uniformly (maintain the image's aspect ratio) so that both dimensions (width and height) of the image will be equal to or larger than the corresponding dimension of the view (minus padding).
   * - `center` - Center the image in the view along both dimensions. If the image is larger than the view, scale it down uniformly so that it is contained in the view.
   * - `stretch` - Scale width and height independently, This may change the aspect ratio of the image.
   * - `repeat` - Repeat the image to cover the frame of the view. The image will keep its size and aspect ratio, unless it is larger than the view, in which case it will be scaled down uniformly so that it is contained in the view.
   */
  resizeMode?: resizeModes;
  imageBorderRadius?: number;
  childre?: Element,
}

/**
 * Imagebackground
 *
 * This component uses FastImage as the image render engine.
 * If you followed the image caching using FastImage then this would work well with you. Adds image background on your component.
 * 
 * See https://react-native-halcyon.github.io/documentation/docs/components/image-background
 */
export class ImageBackground extends PureComponent<ImageBackgroundProps> { }

export interface InputProps extends TextInputProps, TextInputIOSProps {
  backdropColor?: String;
  /**
   * if `true` `elevation` prop will be required for the `shadow` to work.
   */
  shadow?: Boolean;
  /**
   * `nullified` if `shadow` is `false`. (so it is dependent on the `shadow` prop).
   * 
   * `value` must be 1 or greater for `shadow` to work.
   */
  elevation?: number;
  spacing?: number;
  verticalSpacing?: number;
  borderRadius?: number;
  /**
   * if it has a `value` you need to pass `borderColor` prop otherwise it will return a `transparent` color.
   */
  borderWidth?: number;
  borderColor?: String;
  /**
   * if `true` `leftIcon` can be tapped.
   */
  leftButton?: Boolean;
  /**
   * callback if `leftButton` is true nad tapped.
   */
  leftButtonPress?: () => void;
  leftIcon?: String;
  leftIconSize?: number;
  leftIconColor?: String;
  leftBorderColor?: String;
  leftBorderThickness?: number;
  rightButton?: Boolean;
  rightButtonPress?: () => void;
  rightIcon?: String;
  rightIconSize?: number;
  rightIconColor?: String;
  rightBorderColor?: String;
  rightBorderThickness?: number;
  placeholderColor?: String;
  secureText?: Boolean;
  /**
   * if `true` the `placeholder` and `value` text will be on the center.
   */
  center?: Boolean;
  /**
   * size of the font for `placeholder` and `value`.
   */
  inputSize?: number;
  /**
    * This can be one of the following values:
    *
    * - `light` -  Thinner font
    * - `bold` - Heavier font
    * - `regular` - Common font
    * 
    * > See `config/constants/themes/Fonts.js` for font values.
    * > The default is `regular`.
    */
  fontType?: fontTypes;
  fontColor?: String;
  containerStyle?: StyleProp<ViewStyle>
  inputStyle?: StyleProp<TextStyle>
}

/**
 * Input
 *
 * Made from TextInput. TextInput props are supported.
 * 
 * See https://react-native-halcyon.github.io/documentation/docs/components/input
 */
export class Input extends PureComponent<InputProps> { }

export interface ContentProps extends ScrollViewProps, KeyboardAwareProps {
  backgroundColor?: String,
  children?: Element,
}

export class Content extends PureComponent<ContentProps> { }

export interface HeaderProps {
  backgroundColor?: String,
  left?: String | Element,
  leftColor?: String,
  leftPress?: Function,
  center?: String | Element,
  centerSize?: Number,
  centerFontType?: fontTypes,
  centerColor?: String,
  right?: String | Element,
  rightColor?: String,
  rightPress?: Function,
  leftTestId?: String,
  rightTestId?: String,
  leftSize?: Number,
  rightSize?: Number,
}

export class Header extends PureComponent<HeaderProps> { }

type drawerType = "overlay" | "static" | "displace"
type drawerGestures = true | false | "open" | "closed"
type drawerPositions = "left" | "right" | "top" | "bottom"

export interface DrawerProps {
  content: Element,
  type: drawerType,
  open: boolean,
  openDrawerOffset: number | Function,
  closedDrawerOffset: number | Function,
  disabled: boolean,
  styles: object,
  tweenHandler?: Function,
  tweenDuration?: number,
  tweenEasing?: String,
  onOpen?: Function,
  onOpenStart?: Function,
  onClose?: Function,
  onCloseStart?: Function,
  captureGestures?: drawerGestures,
  acceptDoubleTap?: boolean,
  acceptTap?: boolean,
  acceptPan?: boolean,
  tapToClose?: boolean,
  negotiatePan?: boolean,
  panThreshold?: number,
  panOpenMask?: number,
  panCloseMask?: number,
  initializeOpen?: boolean,
  side?: drawerPositions,
  useInteractionManager?: boolean,
  elevation?: number,
}

/**
 * Drawer
 * 
 * Component that has been modified for easier usage.
 */
export class Drawer extends PureComponent<DrawerProps> { }

export interface RNParallaxProps extends ScrollViewProps {
  renderContent: Function,
  renderNavBar?: Function,
  backgroundColor?: String,
  backgroundImage?: customImageCache | String,
  navbarColor?: String,
  title?: String | Element,
  titleSize?: Number,
  titleColor?: String,
  titleFontType?: String,
  titleStyle?: Object,
  headerTitleStyle?: Object,
  headerMaxHeight?: Number,
  headerMinHeight?: Number,
  extraScrollHeight?: Number,
  backgroundImageScale?: Number,
  contentContainerStyle?: Object,
  innerContainerStyle?: Object,
  scrollViewStyle?: Object,
  containerStyle?: Object,
  alwaysShowTitle?: Boolean,
  alwaysShowNavBar?: Boolean,
  statusBarColor?: String,
  scrollViewProps?: Object,
  resize?: resizeModes,
}

/**
 * RNParallax
 * 
 * Component that provides Parallax effect using ScrollView component
 * 
 * To test just pass testID to scrollViewProps
 */
export class RNParallax extends PureComponent<RNParallaxProps> { }

export class NetworkProvider extends Component<any> { }