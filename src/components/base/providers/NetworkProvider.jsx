/* eslint-disable react/no-unused-state */
import React, { Component, createContext } from 'react'

import NetInfo from '@react-native-community/netinfo';

export const NetworkContext = createContext({
  type: 'none',
  connected: false,
});

class NetworkProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'none',
      connected: false,
    }
  }

  componentDidMount() {
    NetInfo.fetch().then(state => {
      this.setState({
        type: state.type,
        connected: state.isInternetReachable,
      })
    });

    this.subscribe = NetInfo.addEventListener(state => {
      this.setState({
        type: state.type,
        connected: state.isInternetReachable,
      })
    });
  }

  componentWillUnmount() {
    this.subscribe();
  }

  render() {
    const { children } = this.props;
    return (
      <NetworkContext.Provider value={this.state}>
        {children}
      </NetworkContext.Provider>
    )
  }
}

export default NetworkProvider;
