/* eslint-disable import/no-cycle */
/* eslint-disable react-native/no-inline-styles */
import React, { PureComponent } from 'react'

import {
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import Proptypes from 'prop-types'

import { COLOR, GLOBAL, FONT } from '@themes'
import baseConfig from '../../base-config'

class Input extends PureComponent {
  renderLeftIcon = (button, onPress, name, size, color, spacing, borderColor, thickness, testID) => {
    if (name !== "") {
      if (button) {
        return (
          <TouchableOpacity
            onPress={onPress}
            style={[GLOBAL.row, GLOBAL.alignItemsCenter]}
            testID={testID}
          >
            <Icon name={name} size={size} color={color} />

            {borderColor !== "" && <View style={{ width: spacing }} />}

            {borderColor !== "" && (
              <View style={{
                borderColor,
                borderRightWidth: thickness,
                height: size,
              }}
              />
            )}
          </TouchableOpacity>
        )
      }
      return (
        <View
          style={[GLOBAL.row, GLOBAL.alignItemsCenter]}
          testID={testID}
        >
          <Icon name={name} size={size} color={color} />

          {borderColor !== "" && <View style={{ width: spacing }} />}

          {borderColor !== "" && (
            <View style={{
              borderColor,
              borderRightWidth: thickness,
              height: size,
            }}
            />
          )}
        </View>
      )
    }

    return (
      <View />
    )
  }

  renderRightIcon = (button, onPress, name, size, color, spacing, borderColor, thickness, testID) => {
    if (name !== "") {
      if (button) {
        return (
          <TouchableOpacity
            onPress={onPress}
            style={[GLOBAL.row, GLOBAL.alignItemsCenter]}
            testID={testID}
          >
            {borderColor !== "" && (
              <View style={{
                borderColor,
                borderLeftWidth: thickness,
                height: size,
              }}
              />
            )}

            {borderColor !== "" && <View style={{ width: spacing }} />}

            <Icon name={name} size={size} color={color} />

          </TouchableOpacity>
        )
      }
      return (
        <View
          style={[GLOBAL.row, GLOBAL.alignItemsCenter]}
          testID={testID}
        >
          {borderColor !== "" && (
            <View style={{
              borderColor,
              borderRightWidth: thickness,
              height: size,
            }}
            />
          )}

          {borderColor !== "" && <View style={{ width: spacing }} />}

          <Icon name={name} size={size} color={color} />
        </View>
      )
    }

    return (
      <View />
    )
  }

  identifyFontType = fontType => {
    switch (fontType) {
      case 'light':
        return FONT.light;
      case 'bold':
        return FONT.bold;
      case 'lightItalic':
        return FONT.lightItalic;
      case 'italic':
        return FONT.italic;
      case 'boldItalic':
        return FONT.boldItalic;
      default:
        return FONT.regular;
    }
  }

  shadowPlacement = (shadow, elevation) => {
    if (shadow) {
      return {
        shadowColor: COLOR.GRAY,
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation,
      }
    }
    return {
      shadowColor: COLOR.GRAY,
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0,
      shadowRadius: 0,
      elevation: 0,
    }
  }

  render() {
    const {
      backdropColor,
      shadow,
      elevation,
      spacing,
      borderRadius,
      borderWidth,
      borderColor,
      leftButton,
      leftButtonPress,
      leftIcon,
      leftIconSize,
      leftIconColor,
      leftBorderColor,
      leftBorderThickness,
      leftTestId,
      rightButton,
      rightButtonPress,
      rightIcon,
      rightIconSize,
      rightIconColor,
      rightBorderColor,
      rightBorderThickness,
      rightTestId,
      value,
      onChangeText,
      placeholder,
      placeholderColor,
      secureText,
      center,
      inputSize,
      fontType,
      fontColor,
      verticalSpacing,
      containerStyle,
      inputStyle,
      ...props
    } = this.props;

    return (
      <View
        style={{
          backgroundColor: backdropColor,
          paddingLeft: spacing,
          paddingRight: spacing,
          ...GLOBAL.spaceBetween,
          ...GLOBAL.row,
          ...GLOBAL.alignItemsCenter,
          borderRadius,
          ...this.shadowPlacement(shadow, elevation),
          borderWidth,
          borderColor,
          ...containerStyle,
        }}
      >
        <View style={GLOBAL.flex0}>
          {this.renderLeftIcon(leftButton, leftButtonPress, leftIcon, leftIconSize, leftIconColor, spacing, leftBorderColor, leftBorderThickness, leftTestId)}
        </View>
        <View style={GLOBAL.flex1}>
          <TextInput
            style={{
              textAlign: center ? 'center' : 'left',
              fontFamily: this.identifyFontType(fontType),
              fontSize: inputSize,
              color: fontColor,
              paddingLeft: spacing,
              paddingRight: spacing,
              paddingTop: verticalSpacing,
              paddingBottom: verticalSpacing,
              margin: 0,
              ...inputStyle,
            }}
            placeholderTextColor={placeholderColor}
            value={value}
            placeholder={placeholder}
            secureTextEntry={secureText}
            underlineColorAndroid="transparent"
            onChangeText={onChangeText}
            autoCapitalize="none"
            {...props}
          />
        </View>
        <View style={GLOBAL.flex0}>
          {this.renderRightIcon(rightButton, rightButtonPress, rightIcon, rightIconSize, rightIconColor, spacing, rightBorderColor, rightBorderThickness, rightTestId)}
        </View>
      </View>
    );
  }
}

Input.propTypes = {
  backdropColor: Proptypes.string,
  shadow: Proptypes.bool,
  elevation: Proptypes.number,
  spacing: Proptypes.number,
  verticalSpacing: Proptypes.number,
  borderRadius: Proptypes.number,
  borderWidth: Proptypes.number,
  borderColor: Proptypes.string,
  leftButton: Proptypes.bool,
  leftButtonPress: Proptypes.func,
  leftIcon: Proptypes.string,
  leftIconSize: Proptypes.number,
  leftIconColor: Proptypes.string,
  leftBorderColor: Proptypes.string,
  leftBorderThickness: Proptypes.number,
  rightButton: Proptypes.bool,
  rightButtonPress: Proptypes.func,
  rightIcon: Proptypes.string,
  rightIconSize: Proptypes.number,
  rightIconColor: Proptypes.string,
  rightBorderColor: Proptypes.string,
  rightBorderThickness: Proptypes.number,
  value: Proptypes.string.isRequired,
  onChangeText: Proptypes.func.isRequired,
  placeholder: Proptypes.string,
  placeholderColor: Proptypes.string,
  secureText: Proptypes.bool,
  center: Proptypes.bool,
  inputSize: Proptypes.number,
  fontType: Proptypes.string,
  fontColor: Proptypes.string,
  containerStyle: Proptypes.object,
  inputStyle: Proptypes.object,
  leftTestId: Proptypes.string,
  rightTestId: Proptypes.string,
}

Input.defaultProps = {
  backdropColor: baseConfig.input.backdropColor,
  shadow: baseConfig.input.shadow,
  elevation: baseConfig.input.elevation,
  spacing: baseConfig.input.spacing,
  verticalSpacing: baseConfig.input.verticalSpacing,
  borderRadius: baseConfig.input.borderRadius,
  borderWidth: baseConfig.input.borderWidth,
  borderColor: baseConfig.input.borderColor,
  leftButton: baseConfig.input.leftButton,
  leftButtonPress: baseConfig.input.leftButtonPress,
  leftIcon: baseConfig.input.leftIcon,
  leftIconSize: baseConfig.input.leftIconSize,
  leftIconColor: baseConfig.input.leftIconColor,
  leftBorderColor: baseConfig.input.leftBorderColor,
  leftBorderThickness: baseConfig.input.leftBorderThickness,
  rightButton: baseConfig.input.rightButton,
  rightButtonPress: baseConfig.input.rightButtonPress,
  rightIcon: baseConfig.input.rightIcon,
  rightIconSize: baseConfig.input.rightIconSize,
  rightIconColor: baseConfig.input.rightIconColor,
  rightBorderColor: baseConfig.input.rightBorderColor,
  rightBorderThickness: baseConfig.input.rightBorderThickness,
  placeholder: baseConfig.input.placeholder,
  placeholderColor: baseConfig.input.placeholderColor,
  secureText: baseConfig.input.secureText,
  center: baseConfig.input.center,
  inputSize: baseConfig.input.inputSize,
  fontType: baseConfig.input.fontType,
  fontColor: baseConfig.input.fontColor,
  containerStyle: baseConfig.input.containerStyle,
  inputStyle: baseConfig.input.inputStyle,
  leftTestId: baseConfig.input.leftTestId,
  rightTestId: baseConfig.input.rightTestId,
}

export default Input;
