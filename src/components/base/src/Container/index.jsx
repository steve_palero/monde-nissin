import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
  Platform,
  Dimensions,
} from 'react-native'

import LinearGradient from 'react-native-linear-gradient';

import PropTypes from 'prop-types'
import baseConfig from 'components/base/base-config';

const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 0,
  },
})

class Container extends PureComponent {
  render() {
    const {
      gradient,
      start,
      end,
      colors,
      style,
      children,
      ...props
    } = this.props;
    if (gradient) {
      return (
        <LinearGradient
          start={start}
          end={end}
          colors={colors}
          style={{
            ...styles.container,
            ...style,
            height: Platform.OS === 'ios' ? deviceHeight : deviceHeight - 20,
          }}
          {...props}
        >
          {children}
        </LinearGradient>
      )
    }
    return (
      <View
        style={{
          ...styles.container,
          ...style,
          height: Platform.OS === 'ios' ? deviceHeight : deviceHeight - 20,
        }}
        {...props}
      >
        {children}
      </View>
    )
  }
}

Container.propTypes = {
  gradient: PropTypes.bool,
  start: PropTypes.object,
  end: PropTypes.object,
  colors: PropTypes.array,
  style: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
}

Container.defaultProps = {
  gradient: baseConfig.container.gradient,
  start: baseConfig.container.start,
  end: baseConfig.container.end,
  colors: baseConfig.container.colors,
  style: baseConfig.container.style,
  children: baseConfig.container.children,
}


export default Container;
