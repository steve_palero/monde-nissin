/* eslint-disable import/no-cycle */
/* eslint-disable react-native/no-inline-styles */
import React, { PureComponent } from 'react'

import {
  View,
} from 'react-native'

import PropTypes from 'prop-types'
import baseConfig from 'components/base/base-config';

class Space extends PureComponent {
  spaceCalculations = (horizontal, size) => {
    if (horizontal) {
      return {
        width: size,
      }
    }
    return {
      height: size,
    }
  }

  render() {
    const {
      horizontal,
      size,
      color,
    } = this.props;
    return (
      <View
        style={[this.spaceCalculations(horizontal, size), { backgroundColor: color }]}
      />
    )
  }
}

Space.propTypes = {
  horizontal: PropTypes.bool,
  size: PropTypes.number,
  color: PropTypes.string,
}

Space.defaultProps = {
  horizontal: baseConfig.space.horizontal,
  size: baseConfig.space.size,
  color: baseConfig.space.color,
}

export default Space;
