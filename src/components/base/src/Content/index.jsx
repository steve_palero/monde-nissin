/* eslint-disable import/no-cycle */
import React, { PureComponent } from 'react'

import { View, StyleSheet } from 'react-native'

import PropTypes from 'prop-types';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import baseConfig from 'components/base/base-config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

class Content extends PureComponent {
  render() {
    const {
      backgroundColor,
      children,
      ...props
    } = this.props;
    return (
      <View
        style={[
          styles.container,
          {
            backgroundColor,
          },
        ]}
      >
        <KeyboardAwareScrollView
          automaticallyAdjustContentInsets={false}
          enableOnAndroid
          extraHeight={5}
          {...props}
        >
          {children}
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

Content.propTypes = {
  backgroundColor: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
}

Content.defaultProps = {
  backgroundColor: baseConfig.content.backgroundColor,
  children: baseConfig.content.children,
}

export default Content;
