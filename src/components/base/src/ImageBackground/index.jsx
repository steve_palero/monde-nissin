/* eslint-disable import/no-cycle */
import React, { PureComponent } from 'react'

import {
  StyleSheet,
  View,
} from 'react-native'

import FastImage from 'react-native-fast-image'

import PropTypes from 'prop-types';
import baseConfig from 'components/base/base-config';

class ImageBackground extends PureComponent {
  resize = () => {
    const { resizeMode } = this.props;
    if (resizeMode === 'contain') {
      return FastImage.resizeMode.contain
    }
    if (resizeMode === 'cover') {
      return FastImage.resizeMode.cover
    }
    if (resizeMode === 'center') {
      return FastImage.resizeMode.center
    }
    if (resizeMode === 'stretch') {
      return FastImage.resizeMode.stretch
    }
    if (resizeMode === 'repeat') {
      return FastImage.resizeMode.repeat
    }
    return FastImage.resizeMode.contain;
  }

  render() {
    const {
      children,
      style,
      uri,
      imageBorderRadius,
      ...props
    } = this.props;

    return (
      <View
        accessibilityIgnoresInvertColors
        style={style}
      >
        <FastImage
          style={[
            StyleSheet.absoluteFill,
            {
              borderRadius: imageBorderRadius,
            },
          ]}
          source={uri}
          resizeMode={this.resize()}
          {...props}
        />
        {children}
      </View>
    );
  }
}

ImageBackground.propTypes = {
  children: PropTypes.any,
  style: PropTypes.any.isRequired,
  uri: PropTypes.any.isRequired,
  resizeMode: PropTypes.string,
  imageBorderRadius: PropTypes.number,
}

ImageBackground.defaultProps = {
  children: baseConfig.imageBackground.children,
  resizeMode: baseConfig.imageBackground.resizeMode,
  imageBorderRadius: baseConfig.imageBackground.imageBorderRadius,
};

export default ImageBackground;
