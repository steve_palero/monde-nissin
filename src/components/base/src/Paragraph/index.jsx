/* eslint-disable import/no-cycle */
import React, { PureComponent } from 'react'

import {
  Text,
  TouchableOpacity,
} from 'react-native'

import PropTypes from 'prop-types'

import { FONT } from '@themes';
import baseConfig from '../../base-config';

class Paragraph extends PureComponent {
  identifyFontType = () => {
    const {
      fontType,
    } = this.props;

    switch (fontType) {
      case 'light':
        return FONT.light;
      case 'bold':
        return FONT.bold;
      case 'lightItalic':
        return FONT.lightItalic;
      case 'italic':
        return FONT.italic;
      case 'boldItalic':
        return FONT.boldItalic;
      default:
        return FONT.regular;
    }
  }

  render() {
    const {
      size,
      color,
      text,
      center,
      textStyle,
      tappable,
      onTap,
      testID,
      ...props
    } = this.props;
    if (tappable) {
      return (
        <TouchableOpacity
          testID={testID}
          onPress={onTap}
        >
          <Text
            style={{
              fontSize: size,
              fontFamily: this.identifyFontType(),
              color,
              ...center && { textAlign: 'center' },
              ...textStyle,
            }}
            {...props}
          >
            {text}
          </Text>
        </TouchableOpacity>
      )
    }
    return (
      <Text
        testID={testID}
        style={{
          fontSize: size,
          fontFamily: this.identifyFontType(),
          color,
          ...center && { textAlign: 'center' },
          ...textStyle,
        }}
        {...props}
      >
        {text}
      </Text>
    );
  }
}

Paragraph.propTypes = {
  fontType: PropTypes.string,
  size: PropTypes.number,
  color: PropTypes.string,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  center: PropTypes.bool,
  textStyle: PropTypes.object,
  tappable: PropTypes.bool,
  onTap: PropTypes.func,
  testID: PropTypes.string,
}

Paragraph.defaultProps = {
  fontType: baseConfig.paragraph.fontType,
  size: baseConfig.paragraph.size,
  color: baseConfig.paragraph.color,
  center: baseConfig.paragraph.center,
  textStyle: baseConfig.paragraph.textStyle,
  tappable: baseConfig.paragraph.tappable,
  onTap: baseConfig.paragraph.onTap,
  testID: baseConfig.paragraph.testID,
}

export default Paragraph;
