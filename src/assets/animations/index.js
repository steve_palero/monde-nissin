/* eslint-disable global-require */
const ANIMATION = {
  loading: require('./src/loading.json'),
  loggingOut: require('./src/logging-out.json'),
}

export default ANIMATION;
