import React from 'react'

import { createStackNavigator } from 'react-navigation-stack';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { COLOR } from '@themes';

import {
  Map,
  SalesHistory,
  Members,
  Inventory,
  AddNew,
  Profile,
  QRScan,
} from '@screens'

const MapStack = createStackNavigator({
  Map,

  // Drawer screens
  SalesHistory,
  Members,
  Inventory,
  Profile,
  QRScan,

  // Members Stack
  AddNewMember: AddNew,
}, {
  defaultNavigationOptions: {
    headerShown: false,
  },
})

MapStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;

  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
    tabBarOptions: {
      showLabel: false,
      allowFontScaling: false,
      activeBackgroundColor: COLOR.ASH,
      inactiveBackgroundColor: COLOR.DARKER_GRAY,
      activeTintColor: COLOR.LIGHT,
      inactiveTintColor: COLOR.GRAY,
      tabStyle: {
        borderColor: 'transparent',
        borderWidth: 0,
      },
    },
    tabBarIcon: ({ tintColor }) => (
      <Icon
        name="map-marker"
        size={24}
        color={tintColor}
      />
    ),
  }
}

export default MapStack;
