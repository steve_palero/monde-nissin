/* eslint-disable react/prop-types */
import {
  createSwitchNavigator,
  createAppContainer,
} from 'react-navigation';

import { createBottomTabNavigator } from 'react-navigation-tabs';

import * as screens from '@screens'

import DashboardStack from './tabs/Dashboard'
import MapStack from './tabs/Map'
import PersonalStack from './tabs/Personal'

import Auth from './auth'

const Authenticated = createBottomTabNavigator({
  DashboardStack: {
    screen: DashboardStack,
    navigationOptions: {
      tabBarTestID: 'dashboardTab',
    },
  },
  MapStack: {
    screen: MapStack,
    navigationOptions: {
      tabBarTestID: 'mapTab',
    },
  },
  PersonalStack: {
    screen: PersonalStack,
    navigationOptions: {
      tabBarTestID: 'walletTab',
    },
  },
}, {
  initialRouteName: 'DashboardStack',
  tabBarOptions: {
    showLabel: false,
  },
})

const AppSwitchNavigator = createSwitchNavigator({
  Splash: screens.Splash,
  Auth,
  Main: Authenticated,
}, {
  // Default options
  initialRouteName: 'Splash',
})


const AppContainer = createAppContainer(AppSwitchNavigator);

export default AppContainer;
