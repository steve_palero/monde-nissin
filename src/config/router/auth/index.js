import { createStackNavigator } from 'react-navigation-stack'

import { Login } from '@screens'

const Auth = createStackNavigator({
  Login,
}, {
  defaultNavigationOptions: {
    headerShown: false,
  },
})

export default Auth;
