import React, { Component } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native'

import {
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol,
} from 'react-native-responsive-screen'

import {
  Container,
  RNParallax,
  Header,
} from '@base-components';
import { COLOR } from '@themes';
import IMAGES from '@images';

import UserInfo from './components/UserInfo';
import UserSocialInfo from './components/UserSocialInfo';
import UserPosts from './components/UserPosts';

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    backgroundColor: COLOR.LIGHT,
  },
  content: {
    paddingVertical: 20,
    paddingHorizontal: 30,
  },
})

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    loc(this);
  }

  componentWillUnmount() {
    rol();
  }

  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <RNParallax
          headerMinHeight={56}
          headerMaxHeight={hp(40)}
          title=""
          renderNavBar={() => (
            <Header
              left="arrow-left"
              leftPress={() => navigation.goBack()}
              leftTestId="profileHeaderLeftButton"
              backgroundColor={COLOR.TRANSPARENT}
              center=""
              right="chat"
            />
          )}
          navbarColor={COLOR.ASH}
          backgroundImage={IMAGES["steve.png"]}
          contentContainerStyle={styles.container}
          resize="cover"
          scrollViewProps={{
            testID: "profileScrollView",
          }}
          renderContent={() => (
            <View
              testID="profileContent"
              style={styles.content}
            >
              <UserInfo />
              <UserSocialInfo />
              <UserPosts />
            </View>
          )}
        />
      </Container>
    )
  }
}

export default Profile;
