import React, { Component } from 'react'

import {
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import moment from 'moment'

import {
  Container,
  Header,
  Input,
  Paragraph,
  Space,
} from '@base-components';
import { COLOR, GLOBAL } from '@themes';
import { intFormatter } from '@helpers';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.CUSTOM_GRAY,
  },
  listContainer: {
    paddingVertical: 10,
    paddingHorizontal: 30,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  searchButton: {
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.ASH,
    paddingVertical: 6,
  },
  itemContainer: {
    borderRadius: 4,
    backgroundColor: COLOR.LIGHT,
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
})

const transactionsData = [
  {
    id: '1',
    type: 'receive',
    user: '7-eleven',
    amount: 850,
  },
  {
    id: '2',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '3',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '6',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '199',
    type: 'receive',
    user: '7-eleven',
    amount: 850,
  },
  {
    id: '12',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '22',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '25',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '88',
    type: 'receive',
    user: '7-eleven',
    amount: 850,
  },
];

class Transactions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    }
  }

  renderHeader = () => {
    const { search } = this.state;
    return (
      <View style={styles.header}>
        <View style={GLOBAL.flex1}>
          <Input
            value={search}
            onChangeText={v => this.setState({ search: v })}
            borderWidth={1}
            spacing={5}
            placeholder="Search"
            containerStyle={{ backgroundColor: COLOR.LIGHT }}
          />
        </View>
        <View>
          <TouchableOpacity
            style={styles.searchButton}
          >
            <Icon
              name="magnify"
              size={20}
              color={COLOR.LIGHT}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      <View style={styles.row}>
        <Paragraph
          text={item.type === 'receive' ? "Received money from" : "Paid money to"}
          size={12}
        />
        <Paragraph
          text={moment().format('DD, MMM YYYY')}
          size={12}
        />
      </View>
      <Space size={10} />
      <View style={styles.row}>
        <Paragraph
          text={item.user}
          fontType="bold"
        />
        <Paragraph
          text={intFormatter(item.amount)}
          fontType="bold"
          color={item.type === 'receive' ? COLOR.APP_GREEN : COLOR.GOOGLE}
        />
      </View>
    </View>
  )

  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header
          left="arrow-left"
          leftColor={COLOR.WHITE}
          leftPress={() => navigation.goBack()}
          leftTestId="transactionsBack"
          center="Transactions"
        />
        <View style={styles.container}>
          <FlatList
            data={transactionsData}
            renderItem={this.renderItem}
            contentContainerStyle={styles.listContainer}
            ListHeaderComponent={this.renderHeader}
            ItemSeparatorComponent={() => (<Space size={10} />)}
            testID="transactionsList"
          />
        </View>
      </Container>
    )
  }
}

export default Transactions;
