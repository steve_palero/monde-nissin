import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native'

import { Paragraph, Space } from '@base-components'
import { COLOR } from '@themes'


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 40,
    paddingHorizontal: 40,
  },
  bar: {
    width: 80,
    height: 5,
    borderRadius: 5,
    backgroundColor: COLOR.ASH,
  },
})

class UserInfo extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.bar} />
        <Space size={20} />
        <Paragraph
          text="Steve Palero"
          fontType="bold"
          size={20}
          center
        />
        <Paragraph
          text="Davao, PH"
          size={12}
        />

        <Space size={20} />

        <Paragraph
          text="We think everything in this UNIVERSE has to conform to our paradigm of what makes sense."
          center
        />
      </View>
    )
  }
}

export default UserInfo;
