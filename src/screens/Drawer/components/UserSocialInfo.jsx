import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import LinearGradient from 'react-native-linear-gradient'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { Paragraph, Space } from '@base-components'
import { COLOR } from '@themes'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  touchable: {
    flex: 1,
  },
  followButton: {
    flex: 1,
    borderRadius: 20,
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  socialContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  socialInfo: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

const followGradient = ['#0F2027', '#203A43', '#2C5364']

class UserSocialInfo extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.touchable}
        >
          <LinearGradient
            colors={followGradient}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.followButton}
          >
            <Icon
              name="plus"
              size={24}
              color={COLOR.LIGHT}
            />
            <Space horizontal size={5} />
            <Paragraph
              text="Follow"
              fontType="bold"
              size={16}
              color={COLOR.LIGHT}
            />
          </LinearGradient>
        </TouchableOpacity>
        <Space size={20} />
        <View style={styles.socialContainer}>
          <View style={styles.socialInfo}>
            <Paragraph
              text="167"
              fontType="bold"
              size={16}
            />
            <Paragraph
              text="post"
            />
          </View>
          <View style={styles.socialInfo}>
            <Paragraph
              text="3.4K"
              fontType="bold"
              size={16}
            />
            <Paragraph
              text="follower"
            />
          </View>
          <View style={styles.socialInfo}>
            <Paragraph
              text="7.9K"
              fontType="bold"
              size={16}
            />
            <Paragraph
              text="following"
            />
          </View>
        </View>
      </View>
    )
  }
}

export default UserSocialInfo;
