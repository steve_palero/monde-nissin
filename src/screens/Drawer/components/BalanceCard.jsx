import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native'
import { COLOR } from 'config/themes';
import { Paragraph, Space } from '@base-components';

const styles = StyleSheet.create({
  container: {
    borderRadius: 4,
    backgroundColor: COLOR.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
})

class BalanceCard extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <Space size={20} />
        <Paragraph
          text="PHP 1,245.00"
          size={24}
          fontType="bold"
        />
        <Space size={20} />
        <Paragraph
          text="Wallet Balance"
        />
      </View>
    )
  }
}

export default BalanceCard;
