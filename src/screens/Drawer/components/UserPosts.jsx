import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native'
import { Paragraph, Space } from '@base-components'
import { COLOR } from '@themes'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 20,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  post: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: COLOR.ASH,
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginVertical: 10,
  },
})

class UserPosts extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Paragraph
            text="Posts"
            fontType="bold"
          />
          <Paragraph
            text="View all"
            fontType="bold"
            color={COLOR.TEAL_BLUE}
            tappable
          />
        </View>
        <View style={styles.post}>
          <Paragraph
            text="There's Really A Wolf"
            fontType="bold"
            size={16}
          />
          <Space size={5} />
          <Paragraph
            text="is Russ' debut album after 10 years of hard work. The highly awaited album follows up his previous 11 projects over 250 singles."
          />
        </View>
        <View style={styles.post}>
          <Paragraph
            text="There's Really A Wolf"
            fontType="bold"
            size={16}
          />
          <Space size={5} />
          <Paragraph
            text="is Russ' debut album after 10 years of hard work. The highly awaited album follows up his previous 11 projects over 250 singles."
          />
        </View>
        <View style={styles.post}>
          <Paragraph
            text="There's Really A Wolf"
            fontType="bold"
            size={16}
          />
          <Space size={5} />
          <Paragraph
            text="is Russ' debut album after 10 years of hard work. The highly awaited album follows up his previous 11 projects over 250 singles."
          />
        </View>
        <View style={styles.post}>
          <Paragraph
            text="There's Really A Wolf"
            fontType="bold"
            size={16}
          />
          <Space size={5} />
          <Paragraph
            text="is Russ' debut album after 10 years of hard work. The highly awaited album follows up his previous 11 projects over 250 singles."
          />
        </View>
        <View style={styles.post}>
          <Paragraph
            text="There's Really A Wolf"
            fontType="bold"
            size={16}
          />
          <Space size={5} />
          <Paragraph
            text="is Russ' debut album after 10 years of hard work. The highly awaited album follows up his previous 11 projects over 250 singles."
          />
        </View>
      </View>
    )
  }
}

export default UserPosts;
