import React, { Component } from 'react'

import {
  View,
  FlatList,
  StyleSheet,
} from 'react-native'

import moment from 'moment'

import {
  Container,
  Header,
  Paragraph,
  Space,
} from '@base-components';
import { COLOR } from '@themes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  header: {
    borderRadius: 4,
    backgroundColor: COLOR.LIGHT,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
  },
  headerLeft: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerRight: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemContainer: {
    borderRadius: 4,
    backgroundColor: COLOR.LIGHT,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  itemContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  left: {
    flex: 4,
  },
  right: {
    flex: 6,
  },
})

const data = [
  {
    id: '1',
    name: 'Cardo Dalisay',
    received: 200,
  },
  {
    id: '2',
    name: 'Cardo Dalisay',
    received: 200,
  },
  {
    id: '3',
    name: 'Cardo Dalisay',
    received: 200,
  },
  {
    id: '31',
    name: 'Cardo Dalisay',
    received: 200,
  },
  {
    id: '32',
    name: 'Cardo Dalisay',
    received: 200,
  },
  {
    id: '33',
    name: 'Cardo Dalisay',
    received: 200,
  },
  {
    id: '34',
    name: 'Cardo Dalisay',
    received: 200,
  },
  {
    id: '35',
    name: 'Cardo Dalisay',
    received: 200,
  },
]

class Inventory extends Component {
  renderHeader = () => (
    <>
      <View style={styles.header}>
        <View style={styles.headerLeft}>
          <Paragraph
            text="In Stock"
            fontType="bold"
            size={16}
          />
        </View>
        <View style={styles.headerRight}>
          <Paragraph
            text="150 Bottles"
            fontType="bold"
            size={18}
          />
        </View>
      </View>
      <Space size={20} />
      <Paragraph
        text="Transactions: "
      />
      <Space size={10} />
    </>
  )

  renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      <View style={styles.itemContent}>
        <View style={styles.left}>
          <Paragraph
            text="Mobiler:"
            fontType="bold"
          />
        </View>
        <View style={styles.right}>
          <Paragraph
            text={item.name}
          />
        </View>
      </View>
      <View style={styles.itemContent}>
        <View style={styles.left}>
          <Paragraph
            text="Bottles Received:"
            fontType="bold"
          />
        </View>
        <View style={styles.right}>
          <Paragraph
            text={item.received}
          />
        </View>
      </View>
      <View style={styles.itemContent}>
        <View style={styles.left}>
          <Paragraph
            text="Date Received:"
            fontType="bold"
          />
        </View>
        <View style={styles.right}>
          <Paragraph
            text={moment().format('MMMM DD, YYYY')}
          />
        </View>
      </View>
    </View>
  )

  render() {
    const { navigation } = this.props;
    return (
      <Container
        backgroundColor={COLOR.CUSTOM_GRAY}
      >
        <Header
          left="arrow-left"
          leftColor={COLOR.LIGHT}
          leftPress={() => navigation.goBack()}
          leftTestId="inventoryBack"
          center="Inventory"
        />
        <View style={styles.container}>
          <FlatList
            data={data}
            renderItem={this.renderItem}
            contentContainerStyle={styles.listContainer}
            ListHeaderComponent={this.renderHeader}
            ItemSeparatorComponent={() => (
              <Space size={10} />
            )}
            testID="inventoryList"
          />
        </View>
      </Container>
    )
  }
}

export default Inventory;
