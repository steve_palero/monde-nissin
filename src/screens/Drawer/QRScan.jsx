import React, { Component } from 'react'

import {
  View,
  Alert,
  StyleSheet,
} from 'react-native'

import { RNCamera } from 'react-native-camera';

import {
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'

import {
  Container,
  Header,
} from '@base-components';
import { FillAspectToRatio } from '@custom-components';
import { COLOR } from '@themes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraStyle: {
    width: wp(100) * 0.8,
    height: wp(100) * 0.8,
  },
  qrCodeDetector: {
    borderColor: COLOR.RED,
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    justifyContent: 'center',
  },
})

class QRScan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      canDetect: true,
      bounds: null,
    }
  }

  openQRData = e => {
    this.setState({
      bounds: e.barCodes[0].bounds,
    })
    console.warn(`qr code: ${JSON.stringify(e)}`);
    const qrdata = e.barcodes[0].data
    console.log("qrdata :", qrdata)
    Alert.alert(
      'QR Data',
      qrdata,
      [
        {
          text: 'Okay',
          onPress: () => this.setState({ canDetect: true, bounds: null }),
        },
      ],
      { cancelable: false },
    )
  }

  render() {
    const { navigation } = this.props;
    const { canDetect, bounds } = this.state;
    return (
      <Container>
        <Header
          left="arrow-left"
          leftPress={() => navigation.goBack()}
          center="Scan QR"
        />
        <View style={styles.container}>
          <FillAspectToRatio ratio="1:1">
            <RNCamera
              style={styles.cameraStyle}
              ratio="1:1"
              androidCameraPermissionOptions={{
                title: 'Permission to use camera',
                message: 'We need your permission to use your camera',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              onGoogleVisionBarcodesDetected={canDetect ? this.openQRData : null}
              googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.QR_CODE}
              captureAudio={false}
            >
              {bounds ? (
                <View
                  style={[
                    styles.qrCodeDetector,
                    {
                      width: bounds.size.width,
                      height: bounds.size.height,
                      left: bounds.origin.x,
                      top: bounds.origin.y,
                    },
                  ]}
                />
              ) : <View />}
            </RNCamera>
          </FillAspectToRatio>
        </View>
      </Container>
    )
  }
}

export default QRScan;
