import React, { Component } from 'react'

import {
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import {
  Container,
  Header,
  Content,
  Paragraph,
  Input,
  Space,
} from '@base-components';
import { COLOR } from '@themes';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 40,
    paddingHorizontal: 15,
  },
  inputContainer: {
    borderBottomWidth: 1,
    borderBottomColor: COLOR.LIGHT_GRAY,
    paddingVertical: 5,
  },
  footer: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    padding: 10,
    backgroundColor: COLOR.ASH,
  },
})

class AddNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      address: '',
      mobile: '',
      birthday: null,
    }
  }

  render() {
    const { navigation } = this.props;
    const {
      name,
      address,
      mobile,
      birthday,
    } = this.state;
    return (
      <Container>
        <Header
          left="close"
          leftPress={() => navigation.goBack()}
          leftTestId="addNewMemberBack"
          center="Add New Member"
        />
        <Content contentContainerStyle={styles.container}>
          <Paragraph
            text="Customer Name"
            color={COLOR.GRAY}
          />
          <Input
            value={name}
            onChangeText={v => this.setState({ name: v })}
            containerStyle={styles.inputContainer}
            spacing={5}
            testID="addNewMemberNameInput"
          />
          <Space size={15} />
          <Paragraph
            text="Address"
            color={COLOR.GRAY}
          />
          <Input
            value={address}
            onChangeText={v => this.setState({ address: v })}
            containerStyle={styles.inputContainer}
            spacing={5}
            testID="addNewMemberAddressInput"
          />
          <Space size={15} />
          <Paragraph
            text="Contact Number"
            color={COLOR.GRAY}
          />
          <Input
            value={mobile}
            onChangeText={v => this.setState({ mobile: v })}
            containerStyle={styles.inputContainer}
            spacing={5}
            testID="addNewMemberMobileInput"
          />
          <Space size={15} />
          <Paragraph
            text="Birthday"
            color={COLOR.GRAY}
          />
          <Input
            value={birthday}
            onChangeText={v => this.setState({ birthday: v })}
            placeholder="mm/dd/yyyy"
            rightIcon="calendar"
            rightIconColor={COLOR.ASH}
            rightIconSize={30}
            containerStyle={styles.inputContainer}
            spacing={5}
            testID="addNewMemberBirthdayInput"
          />
          <Space size={50} />
          <TouchableOpacity
            style={styles.footer}
            testID="addNewMemberSubmit"
          >
            <Paragraph
              text="SUBMIT"
              fontType="bold"
              color={COLOR.LIGHT}
            />
          </TouchableOpacity>
        </Content>
      </Container>
    )
  }
}

export default AddNew;
