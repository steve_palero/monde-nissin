import React, { Component } from 'react'

import {
  View,
  StyleSheet,
  FlatList,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import moment from 'moment'

import {
  Container,
  Header,
  Paragraph,
  Space,
} from '@base-components'
import { COLOR } from '@themes'
import { intFormatter } from '@helpers'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  datePicker: {
    borderWidth: 0.7,
    borderColor: COLOR.LIGHT_GRAY,
    backgroundColor: COLOR.LIGHT,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 2,
    marginBottom: 10,
  },
  itemContainer: {
    borderWidth: 1,
    borderColor: COLOR.LIGHT_GRAY,
    borderRadius: 4,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: COLOR.LIGHT,
  },
  itemContent: {
    paddingVertical: 4,
  },
  itemRow: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  flexed: {
    flex: 1,
    paddingVertical: 5,
  },
})

const data = [
  {
    id: '1',
    sale: 1290,
    commision: 490,
    transactions: 45,
  },
  {
    id: '2',
    sale: 1290,
    commision: 490,
    transactions: 45,
  },
  {
    id: '3',
    sale: 1290,
    commision: 490,
    transactions: 45,
  },
  {
    id: '4',
    sale: 1290,
    commision: 490,
    transactions: 45,
  },
  {
    id: '6',
    sale: 1290,
    commision: 490,
    transactions: 45,
  },
  {
    id: '21',
    sale: 1290,
    commision: 490,
    transactions: 45,
  },
];

class SalesHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // date: null,
    }
  }

  renderHeader = () => (
    <View style={styles.datePicker}>
      <Paragraph
        text="mm/dd/yyyy"
        color={COLOR.GRAY}
        fontType="bold"
      />
      <Icon
        name="calendar"
        size={24}
        color={COLOR.GRAY}
      />
    </View>
  )

  renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      <Paragraph
        text={moment().format('MMMM DD, YYYY')}
        fontType="bold"
      />
      <Space size={1} color={COLOR.LIGHT_GRAY} />
      <View style={styles.itemContent}>
        <View style={styles.itemRow}>
          <View style={styles.flexed}>
            <View style={styles.itemRow}>
              <View>
                <Paragraph
                  text="Total sale:"
                />
              </View>
              <View>
                <Paragraph
                  text={intFormatter(item.sale)}
                />
              </View>
            </View>
          </View>
          <Space horizontal size={10} />
          <View style={styles.flexed} />
        </View>
        <View style={styles.itemRow}>
          <View style={styles.flexed}>
            <View style={styles.itemRow}>
              <View>
                <Paragraph
                  text="Commision:"
                />
              </View>
              <View>
                <Paragraph
                  text={intFormatter(item.sale)}
                />
              </View>
            </View>
          </View>
          <Space horizontal size={10} />
          <View style={styles.flexed}>
            <View style={styles.itemRow}>
              <View>
                <Paragraph
                  text="Transactions:"
                />
              </View>
              <View>
                <Paragraph
                  text={item.transactions}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  )

  render() {
    const { navigation } = this.props;
    return (
      <Container
        backgroundColor={COLOR.CUSTOM_GRAY}
      >
        <Header
          center="Sales History"
          left="arrow-left"
          leftTestId="salesHistoryBack"
          leftColor={COLOR.LIGHT}
          leftPress={() => navigation.goBack()}
        />
        <View style={styles.container}>
          <FlatList
            data={data}
            renderItem={this.renderItem}
            ListHeaderComponent={this.renderHeader}
            keyExtractor={(item, index) => item.id || index}
            ItemSeparatorComponent={() => (
              <Space size={10} />
            )}
            contentContainerStyle={styles.listContainer}
            testID="salesHistoryList"
          />
        </View>
      </Container>
    )
  }
}

export default SalesHistory;
