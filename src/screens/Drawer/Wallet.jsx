import React, { Component } from 'react'

import {
  TouchableOpacity,
  StyleSheet,
  FlatList,
  View,
} from 'react-native'

import moment from 'moment'

import {
  Container,
  Header,
  Paragraph,
  Space,
  Drawer,
} from '@base-components';
import { COLOR } from 'config/themes';
import { intFormatter } from 'helpers';
import BalanceCard from './components/BalanceCard';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.CUSTOM_GRAY,
  },
  listContainer: {
    paddingVertical: 5,
    paddingHorizontal: 40,
  },
  payButton: {
    borderRadius: 4,
    backgroundColor: COLOR.ASH,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  itemContainer: {
    borderRadius: 4,
    backgroundColor: COLOR.LIGHT,
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
})

const transactionsData = [
  {
    id: '1',
    type: 'receive',
    user: '7-eleven',
    amount: 850,
  },
  {
    id: '2',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '3',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '6',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '12',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '22',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
  {
    id: '25',
    type: 'send',
    user: 'Monde Nissin',
    amount: 500,
  },
];

class Wallet extends Component {
  renderHeader = () => {
    const { navigation } = this.props;
    return (
      <>
        <Paragraph
          text="Pull to refresh"
          center
          size={12}
        />
        <Space size={5} />
        <BalanceCard />
        <Space size={15} />
        <TouchableOpacity style={styles.payButton}>
          <Paragraph
            size={16}
            text="Pay"
            color={COLOR.LIGHT}
          />
        </TouchableOpacity>
        <Space size={30} />
        <View style={styles.header}>
          <Paragraph
            text="Transactions"
            fontType="bold"
          />
          <Paragraph
            text="View All"
            fontType="bold"
            color={COLOR.TEAL_BLUE}
            testID="transactionsAll"
            tappable
            onTap={() => navigation.navigate('Transactions')}
          />
        </View>
      </>
    )
  }

  renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      <View style={styles.row}>
        <Paragraph
          text={item.type === 'receive' ? "Received money from" : "Paid money to"}
          size={12}
        />
        <Paragraph
          text={moment().format('DD, MMM YYYY')}
          size={12}
        />
      </View>
      <Space size={10} />
      <View style={styles.row}>
        <Paragraph
          text={item.user}
          fontType="bold"
        />
        <Paragraph
          text={intFormatter(item.amount)}
          fontType="bold"
          color={item.type === 'receive' ? COLOR.APP_GREEN : COLOR.GOOGLE}
        />
      </View>
    </View>
  )

  render() {
    return (
      <Drawer
        ref={drawerRef => { this.drawer = drawerRef }}
      >
        <Container>
          <Header
            center="Wallet"
            left="menu"
            leftColor={COLOR.WHITE}
            leftPress={() => this.drawer.open()}
            leftTestId="headerLeftButton"
          />
          <View style={styles.container}>
            <FlatList
              data={transactionsData}
              renderItem={this.renderItem}
              ListHeaderComponent={this.renderHeader}
              keyExtractor={(item, index) => item.id || index}
              showsVerticalScrollIndicator={false}
              ItemSeparatorComponent={() => (
                <Space size={10} />
              )}
              contentContainerStyle={styles.listContainer}
            />
          </View>
        </Container>
      </Drawer>
    )
  }
}

export default Wallet;
