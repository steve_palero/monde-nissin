import React, { Component } from 'react'

import {
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import {
  Container,
  Header,
  Paragraph,
  Space,
} from '@base-components';
import { COLOR } from '@themes';
import moment from 'moment';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerButton: {
    paddingVertical: 10,
    paddingHorizontal: 30,
    backgroundColor: COLOR.ASH,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    marginBottom: 10,
  },
  listContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  itemContainer: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: COLOR.LIGHT_GRAY,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: COLOR.LIGHT,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemRowItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
})

const data = [
  {
    id: '1',
    name: {
      firstName: 'Emilio',
      lastName: 'Aguinaldo',
    },
    address: 'B2 L13 Camias St. Pinagbuhatan, Pasig City',
    mobile: '+63923657466',
  },
  {
    id: '2',
    name: {
      firstName: 'Emilio',
      lastName: 'Aguinaldo',
    },
    address: 'B2 L13 Camias St. Pinagbuhatan, Pasig City',
    mobile: '+63923657466',
  },
  {
    id: '3',
    name: {
      firstName: 'Emilio',
      lastName: 'Aguinaldo',
    },
    address: 'B2 L13 Camias St. Pinagbuhatan, Pasig City',
    mobile: '+63923657466',
  },
  {
    id: '4',
    name: {
      firstName: 'Emilio',
      lastName: 'Aguinaldo',
    },
    address: 'B2 L13 Camias St. Pinagbuhatan, Pasig City',
    mobile: '+63923657466',
  },
]

class Members extends Component {
  renderHeader = () => {
    const { navigation } = this.props;
    return (
      <View style={styles.header}>
        <View />
        <TouchableOpacity
          style={styles.headerButton}
          onPress={() => navigation.navigate('AddNewMember')}
          testID="membersAddNew"
        >
          <Paragraph
            text="ADD MEMBER"
            size={16}
            color={COLOR.LIGHT}
          />
        </TouchableOpacity>
      </View>
    )
  }

  renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      <Paragraph
        text={`${item.name.firstName} ${item.name.lastName}`}
        fontType="bold"
      />
      <Space size={4} />
      <View style={styles.row}>
        <Icon
          name="map-marker"
          size={18}
          color={COLOR.ASH}
        />
        <Paragraph
          text={item.address}
        />
      </View>
      <Space size={6} />
      <View style={styles.row}>
        <View style={styles.itemRowItem}>
          <Icon
            name="phone"
            size={18}
            color={COLOR.ASH}
          />
          <Paragraph
            text={item.mobile}
          />
        </View>
        <View style={styles.itemRowItem}>
          <Icon
            name="calendar"
            size={18}
            color={COLOR.ASH}
          />
          <Paragraph
            text={moment().format('MMMM DD, YYYY')}
          />
        </View>
      </View>
    </View>
  )

  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header
          center="Members"
          left="arrow-left"
          leftColor={COLOR.LIGHT}
          leftPress={() => navigation.goBack()}
          leftTestId="membersHeaderBack"
        />
        <View style={styles.container}>
          <FlatList
            data={data}
            renderItem={this.renderItem}
            ListHeaderComponent={this.renderHeader}
            contentContainerStyle={styles.listContainer}
            ItemSeparatorComponent={() => (
              <Space size={10} />
            )}
            testID="membersList"
          />
        </View>
      </Container>
    )
  }
}

export default Members;
