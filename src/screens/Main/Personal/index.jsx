import React, { Component } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native';

import {
  Container,
  Content,
  Drawer,
  Header,
  Paragraph,
  Space,
} from '@base-components';
import { COLOR } from '@themes';
import Charts from './components/Charts';
import GoalToday from './components/GoalToday';
import Note from './components/Note';
import TransactionStatistics from './components/TransactionStatistics';
import { DrawerContent } from '@custom-components';

const styles = StyleSheet.create({
  header: {
    height: 360,
  },
  headerContent: {
    height: 250,
    backgroundColor: COLOR.ASH,
    alignItems: 'center',
    paddingVertical: 40,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  headerFooter: {
    height: 240,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
})

class Personal extends Component {
  render() {
    return (
      <Drawer
        ref={drawerRef => { this.drawer = drawerRef }}
        content={(<DrawerContent />)}
      >
        <Container>
          <Header
            left="menu"
            leftColor={COLOR.WHITE}
            leftPress={() => this.drawer.open()}
            leftTestId="headerLeftButton"
            center="Personal Space"
          />
          <Content backgroundColor={COLOR.DARKER_GRAY}>
            <View style={styles.header}>
              <View style={styles.headerContent}>
                <Paragraph
                  color={COLOR.LIGHT_GRAY}
                  text="Sold"
                  size={18}
                />
                <Paragraph
                  fontType="bold"
                  text="1240"
                  size={30}
                  color={COLOR.LIGHT}
                />
              </View>
              <View style={styles.headerFooter}>
                <Charts />
              </View>
            </View>
            <Space size={20} />
            <GoalToday />
            <Space size={20} />
            <TransactionStatistics />
            <Note />
          </Content>
        </Container>
      </Drawer>
    )
  }
}

export default Personal;
