import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
} from 'react-native'
import { Paragraph, Space } from '@base-components';
import { COLOR } from '@themes';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
  },
  card: {
    width: Dimensions.get('window').width * 0.6,
    padding: 20,
    borderRadius: 8,
    backgroundColor: COLOR.BLUE_GREEN,
  },
})

class GoalToday extends PureComponent {
  render() {
    return (
      <View>
        <Paragraph
          fontType="bold"
          size={20}
          color={COLOR.LIGHT}
          text="Today's Goal"
          textStyle={styles.container}
        />
        <Space size={10} />
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.container}
        >
          <View style={styles.card}>
            <Paragraph
              text="Sell"
              color={COLOR.WHITE}
              size={18}
            />
            <Space size={15} />
            <Paragraph
              fontType="bold"
              size={24}
              color={COLOR.WHITE}
              text="400 Bottles"
            />
          </View>
          <Space horizontal size={20} />
          <View style={styles.card}>
            <Paragraph
              text="Acquire"
              color={COLOR.WHITE}
              size={18}
            />
            <Space size={15} />
            <Paragraph
              fontType="bold"
              size={24}
              color={COLOR.WHITE}
              text="10 Members"
            />
          </View>
        </ScrollView>
        <Space size={30} />
      </View>
    )
  }
}

export default GoalToday;
