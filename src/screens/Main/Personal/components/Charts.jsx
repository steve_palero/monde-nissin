import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
} from 'react-native'

import {
  LineChart,
  BarChart,
} from 'react-native-chart-kit'

import { Paragraph, Space } from '@base-components';
import { COLOR } from '@themes';

const styles = StyleSheet.create({
  scrollContainer: {
    paddingHorizontal: 15,
  },
  // eslint-disable-next-line react-native/no-color-literals
  headerCard: {
    paddingHorizontal: 5,
    paddingVertical: 5,
    backgroundColor: COLOR.APP_CHARTS_PINK,
    borderRadius: 10,
  },
  cardText: {
    paddingLeft: 10,
  },
  chartContainer: {
    marginVertical: 2,
    borderRadius: 4,
  },
})

class Charts extends PureComponent {
  render() {
    return (
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.scrollContainer}
      >
        <View style={styles.headerCard}>
          <Paragraph
            text="Sold"
            color={COLOR.LIGHT}
            textStyle={styles.cardText}
          />
          <Paragraph
            fontType="bold"
            size={22}
            color={COLOR.LIGHT}
            text="Average - 521"
            textStyle={styles.cardText}
          />
          <Space size={1} color={COLOR.WHITE} />
          <LineChart
            data={{
              labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
              datasets: [
                {
                  data: [
                    Math.random() * 1042,
                    Math.random() * 1042,
                    Math.random() * 1042,
                    Math.random() * 1042,
                    Math.random() * 1042,
                    Math.random() * 1042,
                  ],
                  labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                },
              ],
            }}
            width={Dimensions.get('window').width * 0.75} // from react-native
            height={170}
            yAxisLabel=""
            yAxisSuffix=""
            yAxisInterval={1} // optional, defaults to 1
            chartConfig={{
              backgroundColor: COLOR.APP_CHARTS_PINK,
              backgroundGradientFrom: COLOR.APP_CHARTS_PINK,
              backgroundGradientTo: COLOR.APP_CHARTS_PINK,
              decimalPlaces: 0, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              propsForLabels: {
                fill: COLOR.LIGHT,
                fontSize: 12,
                fontWeight: 'bold',
              },
              style: {
                borderRadius: 16,
              },
              propsForDots: {
                r: "6",
                strokeWidth: "2",
                stroke: COLOR.WHITE,
              },
            }}
            // bezier
            style={styles.chartContainer}
          />
        </View>
        <Space horizontal size={25} />
        <View style={styles.headerCard}>
          <Paragraph
            text="Commission"
            color={COLOR.LIGHT}
            textStyle={styles.cardText}
          />
          <Paragraph
            fontType="bold"
            size={22}
            color={COLOR.LIGHT}
            text="Average - ₱ 180"
            textStyle={styles.cardText}
          />
          <Space size={1} color={COLOR.WHITE} />
          <BarChart
            style={styles.chartContainer}
            data={{
              labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
              datasets: [
                {
                  data: [
                    Math.random() * 360,
                    Math.random() * 360,
                    Math.random() * 360,
                    Math.random() * 360,
                    Math.random() * 360,
                    Math.random() * 360,
                  ],
                  labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                },
              ],
            }}
            width={Dimensions.get('window').width * 0.75} // from react-native
            height={170}
            chartConfig={{
              backgroundColor: COLOR.APP_CHARTS_PINK,
              backgroundGradientFrom: COLOR.APP_CHARTS_PINK,
              backgroundGradientTo: COLOR.APP_CHARTS_PINK,
              decimalPlaces: 0, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              propsForLabels: {
                fill: COLOR.LIGHT,
                fontSize: 12,
                fontWeight: 'bold',
              },
              style: {
                borderRadius: 16,
              },
              propsForDots: {
                r: "6",
                strokeWidth: "2",
                stroke: COLOR.WHITE,
              },
            }}
          />
        </View>
        <Space horizontal size={25} />
        <View style={styles.headerCard}>
          <Paragraph
            text="Order"
            color={COLOR.LIGHT}
            textStyle={styles.cardText}
          />
          <Paragraph
            fontType="bold"
            size={22}
            color={COLOR.LIGHT}
            text="Average - 300"
            textStyle={styles.cardText}
          />
          <Space size={1} color={COLOR.WHITE} />
          <LineChart
            data={{
              labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
              datasets: [
                {
                  data: [
                    Math.random() * 600,
                    Math.random() * 600,
                    Math.random() * 600,
                    Math.random() * 600,
                    Math.random() * 600,
                    Math.random() * 600,
                  ],
                  labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                },
              ],
            }}
            width={Dimensions.get('window').width * 0.75} // from react-native
            height={170}
            yAxisLabel=""
            yAxisSuffix=""
            yAxisInterval={1} // optional, defaults to 1
            chartConfig={{
              backgroundColor: COLOR.APP_CHARTS_PINK,
              backgroundGradientFrom: COLOR.APP_CHARTS_PINK,
              backgroundGradientTo: COLOR.APP_CHARTS_PINK,
              decimalPlaces: 0, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              propsForLabels: {
                fill: COLOR.LIGHT,
                fontSize: 12,
                fontWeight: 'bold',
              },
              style: {
                borderRadius: 16,
              },
              propsForDots: {
                r: "6",
                strokeWidth: "2",
                stroke: COLOR.WHITE,
              },
            }}
            bezier
            style={styles.chartContainer}
          />
        </View>
      </ScrollView>
    )
  }
}

export default Charts;
