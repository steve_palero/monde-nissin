import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native'
import { Paragraph, Space } from '@base-components';
import { COLOR } from '@themes';

const styles = StyleSheet.create({
  transactionStatCard: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 8,
    backgroundColor: COLOR.PURPLE,
    marginHorizontal: 15,
  },
  transactionStatContent: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  transactionStatItem: {
    justifyContent: 'center',
    padding: 15,
  },
})

class TransactionStatistics extends PureComponent {
  render() {
    return (
      <View style={styles.transactionStatCard}>
        <Paragraph
          fontType="bold"
          text="Transaction Statistics"
          color={COLOR.LIGHT}
          size={20}
        />
        <Space size={10} />
        <Space size={1} color={COLOR.DARKER_GRAY} />
        <Space size={10} />
        <View style={styles.transactionStatContent}>
          <View style={styles.transactionStatItem}>
            <Paragraph
              text="Members"
              color={COLOR.LIGHT}
            />
            <Paragraph
              fontType="bold"
              size={20}
              color={COLOR.LIGHT}
              text="120"
            />
          </View>
          <View style={styles.transactionStatItem}>
            <Paragraph
              text="Walk ins"
              color={COLOR.LIGHT}
            />
            <Paragraph
              fontType="bold"
              size={20}
              color={COLOR.LIGHT}
              text="120"
            />
          </View>
          <View style={styles.transactionStatItem}>
            <Paragraph
              text="New Members"
              color={COLOR.LIGHT}
            />
            <Paragraph
              fontType="bold"
              size={20}
              color={COLOR.LIGHT}
              text="120"
            />
          </View>
        </View>
      </View>
    )
  }
}

export default TransactionStatistics;
