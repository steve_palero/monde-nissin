import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native'
import { Paragraph, Space } from '@base-components';
import { COLOR } from '@themes';

const styles = StyleSheet.create({
  container: {
    margin: 15,
  },
  noteContainer: {
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: COLOR.LIGHT,
  },
})

class Note extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <Paragraph
          fontType="bold"
          size={20}
          color={COLOR.WHITE}
          text="Note"
        />
        <Space size={10} />
        <View style={styles.noteContainer}>
          <Paragraph
            fontType="bold"
            size={18}
            text="Title"
          />
          <Paragraph
            size={16}
            text="If you want to know what a man's like, take a good look at how he treats his inferiors, not his equals."
          />
        </View>
      </View>
    )
  }
}

export default Note;
