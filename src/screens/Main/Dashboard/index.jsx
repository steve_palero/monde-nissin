import React, { Component } from 'react'

import {
  StyleSheet,
} from 'react-native'

import { connect } from 'react-redux'

import {
  Container,
  Content,
  Drawer,
  Header,
} from '@base-components';
import { COLOR } from '@themes';
import StockCard from './components/StockCard';
import ForecastCard from './components/ForecastCard';
import CreditCard from './components/CreditCard';
import CreateTransactionCard from './components/CreateTransactionCard';
import CreateTransactionModal from './components/CreateTransactionModal';
import TransactionAlert from './components/TransactionAlert';
import { DrawerContent } from '@custom-components';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  drawerContent: {
    flex: 1,
    backgroundColor: COLOR.ASH,
  },
})

class Dashboard extends Component {
  render() {
    const { orientation } = this.props;
    console.log('orientation', orientation)
    return (
      <Drawer
        ref={drawerRef => { this.drawer = drawerRef }}
        content={(<DrawerContent />)}
      >
        <Container>
          <Header
            left="menu"
            leftTestId="headerLeftButton"
            leftColor={COLOR.WHITE}
            leftPress={() => this.drawer.open()}
            center="Dashboard"
          />
          <Content
            contentContainerStyle={styles.container}
          >
            <StockCard />
            <ForecastCard />
            <CreditCard />
            <CreateTransactionCard />
          </Content>
          <CreateTransactionModal />
          <TransactionAlert />
        </Container>
      </Drawer>
    )
  }
}

const mapStateToProps = data => ({
  orientation: data.orientationProvider.view,
})

export default connect(mapStateToProps)(Dashboard);
