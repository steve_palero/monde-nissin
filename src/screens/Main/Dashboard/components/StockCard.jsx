import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import moment from 'moment'

import { AnimatedCircularProgress } from 'react-native-circular-progress';

import { Paragraph, Space } from '@base-components';
import { COLOR } from '@themes';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    borderRadius: 4,
    backgroundColor: COLOR.LIGHT,
    marginBottom: 15,
  },
  content: {
    paddingVertical: 5,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  leftContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  reload: {
    paddingVertical: 5,
    paddingHorizontal: 30,
    borderRadius: 3,
    backgroundColor: COLOR.ASH,
  },
})

class StockCard extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <Paragraph
          fontType="bold"
          text={moment().format('MMMM DD, YYYY - dddd')}
        />
        <View style={styles.content}>
          <View styles={styles.leftContent}>
            <TouchableOpacity style={styles.reload}>
              <Paragraph
                color={COLOR.LIGHT}
                size={16}
                text="Reload"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.rightContent}>
            <AnimatedCircularProgress
              size={65}
              width={5}
              fill={64}
              tintColor={COLOR.RED}
              onAnimationComplete={() => console.log('onAnimationComplete')}
              backgroundColor="rgba(255, 59, 48, 0.2)"
              rotation={0}
            >
              {
                fill => (
                  <Paragraph
                    fontType="bold"
                    size={16}
                    color={COLOR.RED}
                    text={fill.toFixed(0)}
                  />
                )
              }
            </AnimatedCircularProgress>
            <Space size={10} />
            <Paragraph
              text="Remaining Stocks"
              size={12}
            />
          </View>
        </View>
      </View>
    )
  }
}

export default StockCard;
