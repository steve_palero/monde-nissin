import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import Modal from 'react-native-modal'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import PropTypes from 'prop-types'

import { connect } from 'react-redux'

import { COLOR } from '@themes'
import { Paragraph, Space, Input } from '@base-components'
import {
  SET_QUANTITY,
  CANCEL_TRANSACTION,
  SAVE_TRANSACTION,
} from '@redux/main/transaction/action-types'

const styles = StyleSheet.create({
  modalContainer: {
    margin: 0,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  modalContent: {
    padding: 20,
    width: '100%',
    backgroundColor: COLOR.LIGHT,
  },
  transactionBody: {
    paddingVertical: 20,
  },
  selectMember: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 15,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: COLOR.LIGHT_GRAY,
  },
  quantity: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  quantityText: {
    flex: 1,
    paddingLeft: 5,
  },
  quantityInput: {
    flex: 2,
  },
  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 20,
  },
  footerButton: {
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    borderRadius: 2,
  },
})

class CreateTransactionModal extends PureComponent {
  renderBody = () => {
    const {
      transaction,
      onQuantityChange,
      onCancel,
      onSave,
    } = this.props;
    if (transaction.type === 'member') {
      return (
        <View
          style={styles.modalContent}
          testID="memberTransactionModal"
        >
          <Paragraph
            text="Member Purchase"
            fontType="bold"
            size={16}
          />
          <Space size={5} />
          <Space size={1} color={COLOR.LIGHT_GRAY} />
          <View style={styles.transactionBody}>
            <View style={styles.selectMember}>
              <Paragraph
                text="Select a member"
              />
              <Icon
                name="menu-down"
                size={30}
                color={COLOR.ASH}
              />
            </View>
          </View>
          <Space size={15} />
          <View style={styles.quantity}>
            <View style={styles.quantityText}>
              <Paragraph
                text="Quantity"
              />
            </View>
            <View style={styles.quantityInput}>
              <Input
                testID="memberPurchaseQuantityInput"
                value={transaction.quantity}
                onChangeText={v => onQuantityChange(v)}
                borderWidth={1}
                borderRadius={4}
                borderColor={COLOR.LIGHT_GRAY}
                spacing={5}
                keyboardType="numeric"
              />
            </View>
          </View>
          <View style={styles.footer}>
            <TouchableOpacity
              style={[
                styles.footerButton,
                {
                  backgroundColor: COLOR.GRAY,
                },
              ]}
              onPress={onCancel}
              testID="memberTransactionCancel"
            >
              <Paragraph
                text="Cancel"
                fontType="bold"
                color={COLOR.LIGHT}
              />
            </TouchableOpacity>
            <Space horizontal size={25} />
            <TouchableOpacity
              style={[
                styles.footerButton,
                {
                  backgroundColor: (transaction.quantity.length <= 0 && transaction.quantity <= 0) ? COLOR.GRAY : COLOR.ASH,
                },
              ]}
              onPress={() => onSave(transaction.quantity, 'member')}
              disabled={transaction.quantity.length <= 0 && transaction.quantity <= 0}
              testID="memberTransactionSave"
            >
              <Paragraph
                text="Save"
                fontType="bold"
                color={COLOR.LIGHT}
              />
            </TouchableOpacity>
          </View>
        </View>
      )
    }
    return (
      <View
        style={styles.modalContent}
        testID="walkInTransactionModal"
      >
        <Paragraph
          text="Walk-in Purchase"
          fontType="bold"
          size={16}
        />
        <Space size={5} />
        <Space size={1} color={COLOR.LIGHT_GRAY} />
        <View style={styles.transactionBody}>
          <View style={styles.quantity}>
            <View style={styles.quantityText}>
              <Paragraph
                text="Quantity:"
                fontType="bold"
              />
            </View>
            <View style={styles.quantityInput}>
              <Input
                testID="walkInPurchaseQuantityInput"
                value={transaction.quantity}
                onChangeText={v => onQuantityChange(v)}
                borderWidth={1}
                borderRadius={4}
                borderColor={COLOR.LIGHT_GRAY}
                spacing={5}
                keyboardType="numeric"
              />
            </View>
          </View>
          <Space size={15} />
          <View style={styles.quantity}>
            <View style={styles.quantityText}>
              <Paragraph
                text="Location:"
                fontType="bold"
              />
            </View>
            <View style={styles.quantityInput}>
              <Paragraph
                text="3456 Rizal St. Pembo, Makati City"
              />
            </View>
          </View>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            style={[
              styles.footerButton,
              {
                backgroundColor: COLOR.GRAY,
              },
            ]}
            onPress={onCancel}
            testID="walkInTransactionCancel"
          >
            <Paragraph
              text="Cancel"
              fontType="bold"
              color={COLOR.LIGHT}
            />
          </TouchableOpacity>
          <Space horizontal size={25} />
          <TouchableOpacity
            style={[
              styles.footerButton,
              {
                backgroundColor: (transaction.quantity.length <= 0 && transaction.quantity <= 0) ? COLOR.GRAY : COLOR.ASH,
              },
            ]}
            onPress={() => onSave(transaction.quantity, 'walk-in')}
            disabled={transaction.quantity.length <= 0 && transaction.quantity <= 0}
            testID="walkInTransactionSave"
          >
            <Paragraph
              text="Save"
              fontType="bold"
              color={COLOR.LIGHT}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const { transaction } = this.props;
    console.log('transaction', transaction)
    return (
      <Modal
        isVisible={transaction.open}
        style={styles.modalContainer}
      >
        {this.renderBody()}
      </Modal>
    )
  }
}

CreateTransactionModal.propTypes = {
  onQuantityChange: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
}

const mapStateToProps = data => ({
  transaction: data.transaction,
})

const mapDispatchToProps = dispatch => ({
  onQuantityChange: quantity => dispatch({
    type: SET_QUANTITY,
    payload: {
      quantity,
    },
  }),
  onCancel: () => dispatch({
    type: CANCEL_TRANSACTION,
  }),
  onSave: (quantity, type) => dispatch({
    type: SAVE_TRANSACTION,
    payload: {
      quantity,
      type,
    },
  }),
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateTransactionModal);
