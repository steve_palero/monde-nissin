import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native'

import { COLOR } from '@themes';
import { Paragraph } from '@base-components';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    borderRadius: 4,
    backgroundColor: COLOR.LIGHT,
    marginBottom: 15,
  },
  content: {
    paddingVertical: 5,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  leftContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
})

class CreditCard extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.leftContent}>
            <Paragraph
              fontType="bold"
              size={16}
              text="Credit Limit"
            />
          </View>
          <View style={styles.rightContent}>
            <Paragraph
              fontType="bold"
              size={24}
              text="100"
            />
          </View>
        </View>
      </View>
    )
  }
}

export default CreditCard;
