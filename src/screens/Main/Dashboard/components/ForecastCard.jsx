import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { COLOR } from '@themes';
import { Paragraph } from '@base-components';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    borderRadius: 4,
    backgroundColor: COLOR.LIGHT,
    marginBottom: 15,
  },
  content: {
    paddingVertical: 5,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  leftContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
})

class ForecastCard extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <Paragraph
          text="Forecast"
          size={16}
          fontType="bold"
          center
        />
        <View style={styles.content}>
          <View style={styles.leftContent}>
            <Icon
              name="chart-line"
              size={70}
              color={COLOR.LIGHT_GREEN}
            />
          </View>
          <View style={styles.rightContent}>
            <Paragraph
              fontType="bold"
              size={24}
              text="85"
            />
            <Paragraph
              text="Suggested order to delear"
              size={12}
            />
          </View>
        </View>
      </View>
    )
  }
}

export default ForecastCard;
