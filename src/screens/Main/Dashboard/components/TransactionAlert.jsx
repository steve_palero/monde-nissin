import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import Modal from 'react-native-modal'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { connect } from 'react-redux'

import PropTypes from 'prop-types'

import { COLOR } from '@themes'
import { Space, Paragraph } from '@base-components'
import { CLOSE_TRANSACTION_ALERT } from '@redux/main/transaction/action-types'

const styles = StyleSheet.create({
  modalContainer: {
    margin: 0,
    paddingHorizontal: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    width: '100%',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.LIGHT,
  },
  iconContainer: {
    padding: 5,
    borderRadius: 40,
  },
  button: {
    width: 100,
    paddingVertical: 10,
    backgroundColor: COLOR.ASH,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 2,
  },
})

class TransactionAlert extends PureComponent {
  render() {
    const {
      transaction,
      onClose,
    } = this.props;
    return (
      <Modal
        isVisible={transaction.openAlert}
        style={styles.modalContainer}
      >
        <View style={styles.container}>
          <View
            style={{
              ...styles.iconContainer,
              backgroundColor: transaction.alertType === 'success' ? COLOR.APP_GREEN : COLOR.RED,
            }}
            testID="transactionAlertIcon"
          >
            <Icon
              name={transaction.alertType === 'success' ? "check" : "close"}
              size={60}
              color={COLOR.LIGHT}
            />
          </View>
          <Space size={20} />
          <Paragraph
            text={transaction.alertType === 'success' ? "Transaction saved" : "Transaction cancelled"}
            fontType="bold"
            size={18}
          />
          <Space size={25} />
          <TouchableOpacity
            style={styles.button}
            onPress={onClose}
            testID="transactionAlertOK"
          >
            <Paragraph
              text="OK"
              color={COLOR.LIGHT}
            />
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }
}

TransactionAlert.propTypes = {
  onClose: PropTypes.func.isRequired,
}

const mapStateToProps = data => ({
  transaction: data.transaction,
})

const mapDispatchToProps = dispatch => ({
  onClose: () => dispatch({ type: CLOSE_TRANSACTION_ALERT }),
})

export default connect(mapStateToProps, mapDispatchToProps)(TransactionAlert);
