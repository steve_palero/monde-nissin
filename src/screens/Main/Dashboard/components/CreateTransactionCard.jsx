import React, { PureComponent } from 'react'

import {
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import { connect } from 'react-redux'

import PropTypes from 'prop-types'

import { COLOR } from '@themes';
import { Paragraph } from '@base-components';
import { CREATE_TRANSACTION } from '@redux/main/transaction/action-types';

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.TRANSPARENT,
    marginBottom: 15,
  },
  title: {
    marginLeft: 20,
    paddingHorizontal: 5,
    backgroundColor: COLOR.CUSTOM_GRAY,
    minWidth: 80,
    zIndex: 2,
    position: 'absolute',
  },
  content: {
    borderRadius: 4,
    borderWidth: 1.5,
    borderColor: COLOR.MEDIUM_GRAY,
    marginTop: 10,
    padding: 20,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  leftContent: {
    flex: 1,
    marginRight: 10,
  },
  rightContent: {
    flex: 1,
    marginLeft: 10,
  },
  button: {
    width: '100%',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.ASH,
    borderRadius: 3,
  },
})

class CreateTransactionCard extends PureComponent {
  render() {
    const {
      open,
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <Paragraph
            text="Create Transaction"
            fontType="bold"
          />
        </View>
        <View style={styles.content}>
          <View style={styles.leftContent}>
            <TouchableOpacity
              testID="memberTransaction"
              style={styles.button}
              onPress={() => open('member')}
            >
              <Paragraph
                text="Member"
                color={COLOR.LIGHT}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.rightContent}>
            <TouchableOpacity
              testID="walkInTransaction"
              style={styles.button}
              onPress={() => open('walk-in')}
            >
              <Paragraph
                text="Walk-In"
                color={COLOR.LIGHT}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

CreateTransactionCard.propTypes = {
  open: PropTypes.func.isRequired,
}

const mapDispatchToProps = dispatch => ({
  open: type => dispatch({
    type: CREATE_TRANSACTION,
    payload: {
      type,
    },
  }),
})

export default connect(null, mapDispatchToProps)(CreateTransactionCard);
