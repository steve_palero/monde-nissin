import React, { Component } from 'react'

import {
  View,
  Switch,
  StyleSheet,
} from 'react-native'

import MapView,
{
  Marker,
  Callout,
  Polygon,
  Polyline,
  Heatmap,
} from 'react-native-maps';

import Geolocation from '@react-native-community/geolocation';

import {
  Container,
  Header,
  Paragraph,
  Space,
  Drawer,
} from '@base-components';
import { COLOR } from '@themes';
import IMAGES from '@images';
import { capitalize } from '@helpers';
import ActionSheet from './components/ActionSheet';
import { DrawerContent } from '@custom-components';

// const mapStyle = require('./mapStyle.json')

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  calloutContainer: {
    backgroundColor: COLOR.TRANSPARENT,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapCallout: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 6,
    backgroundColor: COLOR.LIGHT,
  },
  tooltip: {
    width: 0,
    height: 0,
    backgroundColor: COLOR.TRANSPARENT,
    borderStyle: 'solid',
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderBottomWidth: 10,
    borderLeftColor: COLOR.TRANSPARENT,
    borderRightColor: COLOR.TRANSPARENT,
    borderBottomColor: COLOR.LIGHT,
    transform: [
      { rotate: '180deg' },
    ],
  },
  rightHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  locationText: {
    position: 'absolute',
    bottom: 20,
    left: 20,
  },
})

const polygonCords = [
  {
    latitude: 14.569678,
    longitude: 121.060529,
    weight: 1,
  },
  {
    latitude: 14.571259,
    longitude: 121.061963,
    weight: 1,
  },
  {
    latitude: 14.572403,
    longitude: 121.060664,
    weight: 1,
  },
  {
    latitude: 14.571643,
    longitude: 121.059609,
    weight: 1,
  },
]

const polylineCords = [
  {
    latitude: 14.5747628,
    longitude: 121.0579119,
  },
  {
    latitude: 14.5746995,
    longitude: 121.0578748,
  },
  {
    latitude: 14.5749354,
    longitude: 121.0576355,
  },
  {
    latitude: 14.576398,
    longitude: 121.055894,
  },
  {
    latitude: 14.5765458,
    longitude: 121.0558847,
  },
  {
    latitude: 14.5766279,
    longitude: 121.0559497,
  },
  {
    latitude: 14.5781194,
    longitude: 121.0570822,
  },
  {
    latitude: 14.578376,
    longitude: 121.0572965,
  },
  {
    latitude: 14.5801498,
    longitude: 121.0588477,
  },
  {
    latitude: 14.5800971,
    longitude: 121.058982,
  },
  {
    latitude: 14.5802311,
    longitude: 121.0588457,
  },
  {
    latitude: 14.5805082,
    longitude: 121.0590628,
  },
  {
    latitude: 14.5815848,
    longitude: 121.0599734,
  },
  {
    latitude: 14.5816829,
    longitude: 121.0600376,
  },
  {
    latitude: 14.5819062,
    longitude: 121.0601072,
  },
  {
    latitude: 14.5838839,
    longitude: 121.0600022,
  },
  {
    latitude: 14.5844654,
    longitude: 121.0598688,
  },
  {
    latitude: 14.5844736,
    longitude: 121.0598377,
  },
  {
    latitude: 14.5844766,
    longitude: 121.0586587,
  },
]

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCoveredArea: false,
      mapReady: false,
      initialRegion: null,
    }
  }

  componentDidMount() {
    Geolocation.getCurrentPosition(info => {
      this.setState({
        initialRegion: {
          latitude: info.coords.latitude,
          longitude: info.coords.longitude,
          latitudeDelta: 0.02,
          longitudeDelta: 0.02,
        }
      })
    });
  }

  render() {
    const {
      showCoveredArea,
      mapReady,
      initialRegion,
    } = this.state;
    return (
      <Drawer
        ref={drawerRef => { this.drawer = drawerRef }}
        content={(<DrawerContent />)}
      >
        <Container>
          <Header
            left="menu"
            leftColor={COLOR.WHITE}
            leftPress={() => this.drawer.open()}
            leftTestId="headerLeftButton"
            center=""
            right={(
              <View style={styles.rightHeader}>
                <Switch
                  value={showCoveredArea}
                  onValueChange={v => this.setState({ showCoveredArea: v })}
                  thumbColor={COLOR.GRAY}
                  trackColor={{ false: COLOR.LIGHT_GRAY, true: COLOR.BLACK }}
                  testID="showCoveredArea"
                />
                <Space horizontal size={5} />
                <Paragraph
                  text="Covered Area"
                  color={COLOR.LIGHT}
                  size={12}
                  fontType="bold"
                />
              </View>
            )}
          />
          <View
            style={styles.container}
          >
            <MapView
              style={styles.map}
              initialRegion={initialRegion}
              // customMapStyle={mapStyle}
              loadingEnabled
              toolbarEnabled
              zoomEnabled
              zoomControlEnabled
              showsUserLocation
              onPress={() => this.actionSheet.close()}
              onMapReady={() => this.setState({ mapReady: true })}
            >
              {(showCoveredArea && mapReady) && (
                <Polygon
                  coordinates={polygonCords}
                  fillColor="rgba(90, 200, 250, 0.3)"
                  strokeColor={COLOR.BLUE}
                  strokeWidth={2}
                  testID="coveredArea"
                />
              )}
              <Polyline
                coordinates={polylineCords}
                strokeWidth={10}
                strokeColor={COLOR.RED}
                lineJoin="round"
              />
              <Heatmap
                points={polygonCords}
                radius={30}
              />
              <Marker
                draggable
                coordinate={{
                  latitude: 14.571260561992249,
                  longitude: 121.06087926775217,
                }}
                icon={IMAGES["user-pin.png"]}
                onPress={() => this.actionSheet.setData({
                  name: "Juan Dela Cruz",
                  address: "Some address",
                  percentage: "40%",
                  type: 'user',
                })}
              >
                <Callout
                  tooltip
                  onPress={() => this.actionSheet.open({
                    name: "Juan Dela Cruz",
                    address: "Some address",
                    percentage: "40%",
                    type: 'user',
                  })}
                >
                  <View style={styles.calloutContainer}>
                    <View style={styles.mapCallout}>
                      <Paragraph
                        text={capitalize("Juan Dela Cruz")}
                        size={12}
                        fontType="bold"
                      />
                      <Space size={5} />
                      <Paragraph
                        text="View More Details"
                        fontType="bold"
                        size={12}
                        color={COLOR.BLUE}
                      />
                    </View>
                    <View style={styles.tooltip} />
                  </View>
                </Callout>
              </Marker>
              <Marker
                draggable
                coordinate={{
                  latitude: 14.571758981345365,
                  longitude: 121.0600608587265,
                }}
                image={IMAGES["user-pin.png"]}
                onPress={() => this.actionSheet.setData({
                  name: "Marshall Mathers",
                  address: "2212 Some address",
                  percentage: "40%",
                  type: 'user',
                })}
              >
                <Callout
                  tooltip
                  onPress={() => this.actionSheet.open({
                    name: "Marshall Mathers",
                    address: "2212 Some address",
                    percentage: "40%",
                    type: 'user',
                  })}
                >
                  <View style={styles.calloutContainer}>
                    <View style={styles.mapCallout}>
                      <Paragraph
                        text="Marshall Mathers"
                        size={12}
                        fontType="bold"
                      />
                      <Space size={5} />
                      <Paragraph
                        text="View More Details"
                        fontType="bold"
                        size={12}
                        color={COLOR.BLUE}
                      />
                    </View>
                    <View style={styles.tooltip} />
                  </View>
                </Callout>
              </Marker>
              <Marker
                draggable
                coordinate={{
                  latitude: 14.573260,
                  longitude: 121.059889,
                }}
                onPress={() => this.actionSheet.open({
                  name: "Marshall Mathers",
                  address: "2212 Some address",
                  quantity: "5",
                  mobile: "+639285511763",
                  type: 'order',
                })}
              />
            </MapView>
            <ActionSheet
              ref={actionSheetRef => { this.actionSheet = actionSheetRef }}
            />
          </View>
        </Container>
      </Drawer>
    )
  }
}

export default Map;
