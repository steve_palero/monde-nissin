import React, { Component } from 'react'

import {
  View,
  StyleSheet,
  UIManager,
  LayoutAnimation,
  Platform,
  TouchableOpacity,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { COLOR } from '@themes';
import {
  Paragraph,
  Space,
} from '@base-components';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: COLOR.LIGHT,
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
  header: {
    flexDirection: 'row',
  },
  headerSpace: {
    flex: 1,
  },
  headerbar: {
    flex: 1,
    backgroundColor: COLOR.GRAY,
    height: 5,
  },
  history: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginVertical: 10,
  },
  historyButton: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: COLOR.ASH,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  info: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  order: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  orderLeft: {
    flex: 1,
  },
  orderCenter: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  orderRight: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  callButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 30,
    backgroundColor: COLOR.ASH,
  },
})

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

class ActionSheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      data: {},
    }
  }

  toggle = () => {
    const { expanded } = this.state;
    this.setState({ expanded: !expanded });
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  open = data => {
    this.setState({
      expanded: true,
      data,
    })
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  close = () => {
    this.setState({ expanded: false });
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  }

  setData = data => {
    this.setState({ data })
  }

  renderContent = data => {
    if (data.type === 'user') {
      return (
        <View>
          <View style={styles.header}>
            <View style={styles.headerSpace} />
            <View style={styles.headerbar} />
            <View style={styles.headerSpace} />
          </View>
          <View style={styles.history}>
            <View style={styles.headerSpace} />
            <TouchableOpacity
              style={styles.historyButton}
            >
              <Paragraph
                text="Purchase History"
                color={COLOR.LIGHT}
              />
              <Space horizontal size={15} />
              <Icon
                name="eye"
                size={20}
                color={COLOR.LIGHT}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.info}>
            <Icon
              name="account"
              size={24}
              color={COLOR.ASH}
            />
            <Space horizontal size={5} />
            <Paragraph
              text={data.name}
              fontType="bold"
            />
          </View>
          <View style={styles.info}>
            <Icon
              name="map-marker"
              size={24}
              color={COLOR.ASH}
            />
            <Space horizontal size={5} />
            <Paragraph
              text={data.address}
              fontType="bold"
            />
          </View>
          <View style={styles.info}>
            <Icon
              name="wallet-giftcard"
              size={24}
              color={COLOR.ASH}
            />
            <Space horizontal size={5} />
            <Paragraph
              text={data.percentage}
              fontType="bold"
            />
          </View>
        </View>
      )
    }
    return (
      <View>
        <View style={styles.order}>
          <View style={styles.orderLeft} />
          <View style={styles.orderCenter}>
            <Paragraph
              text="200 m"
              fontType="bold"
              size={18}
            />
          </View>
          <View style={styles.orderRight}>
            <TouchableOpacity
              style={styles.callButton}
            >
              <Icon
                name="phone"
                size={20}
                color={COLOR.LIGHT}
              />
              <Space horizontal size={5} />
              <Paragraph
                text="Call"
                color={COLOR.LIGHT}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.info}>
          <Icon
            name="map-marker"
            size={24}
            color={COLOR.ASH}
          />
          <Space horizontal size={5} />
          <Paragraph
            text={data.address}
            fontType="bold"
          />
        </View>
        <View style={styles.info}>
          <Icon
            name="phone"
            size={24}
            color={COLOR.ASH}
          />
          <Space horizontal size={5} />
          <Paragraph
            text={data.mobile}
            fontType="bold"
          />
        </View>
        <View style={styles.info}>
          <Icon
            name="account"
            size={24}
            color={COLOR.ASH}
          />
          <Space horizontal size={5} />
          <Paragraph
            text={data.name}
            fontType="bold"
          />
        </View>
        <View style={styles.info}>
          <Icon
            name="cart"
            size={24}
            color={COLOR.ASH}
          />
          <Space horizontal size={5} />
          <Paragraph
            text={data.quantity}
            fontType="bold"
          />
        </View>
      </View>
    )
  }

  render() {
    const {
      expanded,
      data,
    } = this.state;
    return (
      <View
        style={[
          styles.container,
          // eslint-disable-next-line react-native/no-inline-styles
          {
            padding: expanded ? 20 : 0,
          },
        ]}
      >
        {expanded && this.renderContent(data)}
      </View>
    )
  }
}

export default ActionSheet;
