import AsyncStorage from "@react-native-community/async-storage";

import {
  put,
  call,
  delay,
  takeLatest,
} from 'redux-saga/effects';

import NavigationService from '@navigationService';
import Config from "react-native-config";
import * as types from './action-types';

function* login(action) {
  yield delay(3000)
  const {
    email,
    password,
  } = action.payload;
  if (email === 'user' && password === '123') {
    // Should be changed to API response from /me
    const session = {
      token: '',
      data: {
        email,
      },
    };
    yield call(AsyncStorage.setItem, Config.SESSION_KEY, JSON.stringify(session))
    yield put({
      type: types.LOGIN_SUCCESS,
      payload: {
        session,
      },
    })
    NavigationService.navigate('Main')
  } else {
    yield put({
      type: types.LOGIN_FAILED,
      payload: {
        error: "Invalid email or password",
      },
    })
  }
}

function* logout() {
  yield AsyncStorage.removeItem(Config.SESSION_KEY)
  yield delay(3000)
  yield put({ type: types.LOGOUT_SUCCESS })
  NavigationService.navigate('Auth')
}

function* restoreSession() {
  try {
    const session = yield call(AsyncStorage.getItem, Config.SESSION_KEY)
    const data = JSON.parse(session);
    if (session != null) {
      yield put({
        type: types.RESTORE_SUCCESS,
        payload: {
          session: data,
        },
      })
      yield delay(2000)
      NavigationService.navigate('Main')
    } else {
      yield put({ type: types.RESTORE_FAILED })
      NavigationService.navigate('Auth')
    }
  } catch (e) {
    console.log('restore error', e)
    yield put({ type: types.RESTORE_FAILED })
    NavigationService.navigate('Auth')
  }
}

const authSaga = [
  takeLatest(types.LOGIN_REQUEST, login),
  takeLatest(types.LOGOUT_PROCESSING, logout),
  takeLatest(types.RESTORE_REQUEST, restoreSession),
];

export default authSaga;
